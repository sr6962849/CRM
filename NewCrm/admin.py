from django.contrib import admin
from .models import NWest, FileModel
from django.utils.safestring import mark_safe


# Register your models here.
class NwestAdmin(admin.ModelAdmin):
    list_display = (
        'client', 'project_name', 'project_id', 'client_requirements', 'region', 'capacity_length', 'design_status',
        'crm_entry_date_formatted', 'installation_type', 'design_assigned', 'ds_assigned_by',
        'remarks'
    )
    list_filter = (
        'client', 'project_name', 'project_id', 'client_requirements', 'region', 'capacity',
        'length_walkway', 'length_handrail', 'design_status',
        'crm_entry_date_formatted', 'installation_type', 'design_assigned', 'ds_assigned_by',
        'remarks')

    def capacity_length(self, obj):
        return mark_safe(f'{obj.capacity}<br>{obj.length_walkway}<br>{obj.length_handrail}')


admin.site.register(NWest, NwestAdmin)
admin.site.register(FileModel)
