from django.db import models


# Create your models here.

class FileModel(models.Model):
    document_file = models.FileField(upload_to='')
    user = models.CharField(max_length=50, blank=True, null=True)
    date_time = models.CharField(max_length=100, blank=True, null=True)


class Rcc_FilModel(models.Model):
    file = models.FileField(upload_to='')
    file_type = models.CharField(max_length=50)  # e.g., GA, BOM, Costing
    nwest_id = models.CharField(max_length=100)  # Or use a more suitable field type
    date_added = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class GroundMount_FilModel(models.Model):
    file = models.FileField(upload_to='')
    file_type = models.CharField(max_length=50)  # e.g., GA, BOM, Costing
    nwest_id = models.CharField(max_length=100)  # Or use a more suitable field type
    date_added = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class IS_FilModel(models.Model):
    file = models.FileField(upload_to='')
    file_type = models.CharField(max_length=50, blank=True, null=True)  # e.g., GA, BOM, Costing
    nwest_id = models.CharField(max_length=100)  # Or use a more suitable field type
    date_added = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class NWest(models.Model):
    sr = models.IntegerField(blank=True, null=True)
    client = models.CharField(max_length=200, blank=True, null=True)
    client_id = models.CharField(max_length=200, blank=True, null=True)
    project_name = models.CharField(max_length=200, blank=True, null=True)
    project_id = models.CharField(max_length=200, blank=True, null=True)
    project_location = models.CharField(max_length=200, blank=True, null=True)
    client_requirements = models.TextField(blank=True, null=True)
    capacity = models.CharField(max_length=200, blank=True, null=True)
    length_walkway = models.CharField(max_length=200, blank=True, null=True)
    length_handrail = models.CharField(max_length=200, blank=True, null=True)
    crm_entry_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    design_intervention = models.TextField(blank=True, null=True)
    installation_type = models.CharField(max_length=200, blank=True, null=True)
    design_request = models.TextField(blank=True, null=True)
    requirement_for_design = models.TextField(blank=True, null=True)
    document_files = models.ManyToManyField(FileModel)
    design_assigned = models.CharField(max_length=50, blank=True, null=True)
    design_status = models.CharField(max_length=50, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    crm_entry_date_formatted = models.CharField(max_length=50, blank=True, null=True)
    sales_status = models.CharField(max_length=50, blank=True, null=True)
    region = models.CharField(max_length=50, blank=True, null=True)
    ds_assigned_by = models.CharField(max_length=100, blank=True, null=True)
    ds_assigned_date = models.CharField(max_length=80, blank=True, null=True)
    enquiry_from = models.CharField(max_length=80, blank=True, null=True)
    r_user = models.CharField(max_length=80, blank=True, null=True)
    post_sales_date = models.DateTimeField(blank=True, null=True)
    post_sales_date_f = models.CharField(max_length=60, blank=True, null=True)
    drop_down_reason = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return f"{self.client} - {self.project_name} - {self.project_id}"


class NWRevision(models.Model):
    sr = models.IntegerField(blank=True, null=True)
    client = models.CharField(max_length=200, blank=True, null=True)
    client_id = models.CharField(max_length=200, blank=True, null=True)
    project_name = models.CharField(max_length=200, blank=True, null=True)
    project_id = models.CharField(max_length=200, blank=True, null=True)
    project_location = models.CharField(max_length=200, blank=True, null=True)
    client_requirements = models.TextField(blank=True, null=True)
    capacity = models.CharField(max_length=200, blank=True, null=True)
    length_walkway = models.CharField(max_length=200, blank=True, null=True)
    length_handrail = models.CharField(max_length=200, blank=True, null=True)
    crm_entry_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    design_intervention = models.TextField(blank=True, null=True)
    installation_type = models.CharField(max_length=200, blank=True, null=True)
    design_request = models.TextField(blank=True, null=True)
    requirement_for_design = models.TextField(blank=True, null=True)
    document_files = models.ManyToManyField(FileModel)
    design_assigned = models.CharField(max_length=50, blank=True, null=True)
    design_status = models.CharField(max_length=50, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    crm_entry_date_formatted = models.CharField(max_length=50, blank=True, null=True)
    sales_status = models.CharField(max_length=50, blank=True, null=True)
    region = models.CharField(max_length=50, blank=True, null=True)
    ds_assigned_by = models.CharField(max_length=100, blank=True, null=True)
    ds_assigned_date = models.CharField(max_length=80, blank=True, null=True)
    revision_number = models.CharField(max_length=10, blank=True, null=True)
    enquiry_from = models.CharField(max_length=80, blank=True, null=True)

    def __str__(self):
        return f"{self.client} - {self.project_name}"
