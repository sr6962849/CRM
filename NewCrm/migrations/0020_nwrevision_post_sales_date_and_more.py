# Generated by Django 4.2.3 on 2024-09-13 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NewCrm', '0019_nwest_r_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='nwrevision',
            name='post_sales_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='nwrevision',
            name='post_sales_date_f',
            field=models.CharField(blank=True, max_length=60, null=True),
        ),
    ]
