# Generated by Django 4.2.3 on 2024-07-24 14:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NewCrm', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='nwest',
            name='design_assigned',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
