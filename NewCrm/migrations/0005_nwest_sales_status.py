# Generated by Django 4.2.3 on 2024-08-05 11:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NewCrm', '0004_nwest_crm_entry_date_formatted'),
    ]

    operations = [
        migrations.AddField(
            model_name='nwest',
            name='sales_status',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
