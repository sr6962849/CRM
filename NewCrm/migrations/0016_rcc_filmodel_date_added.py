# Generated by Django 4.2.3 on 2024-08-26 06:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NewCrm', '0015_nwest_enquiry_from'),
    ]

    operations = [
        migrations.AddField(
            model_name='rcc_filmodel',
            name='date_added',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
