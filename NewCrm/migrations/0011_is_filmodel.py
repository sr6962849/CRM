# Generated by Django 4.2.3 on 2024-08-19 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NewCrm', '0010_groundmount_filmodel_alter_rcc_filmodel_file'),
    ]

    operations = [
        migrations.CreateModel(
            name='IS_FilModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='')),
                ('file_type', models.CharField(max_length=50)),
                ('nwest_id', models.CharField(max_length=100)),
            ],
        ),
    ]
