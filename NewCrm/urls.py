from django.urls import path

from . import views

urlpatterns = [
    path('change_password', views.change_password, name='change_password'),
    path('newInsert', views.newInsert, name="newInsert"),
    path('NInsertData', views.NInsertData, name="NInsertData"),
    path('newDesign', views.newDesign, name="newDesign"),
    path('newdesShow', views.newdesShow, name="newdesShow"),
    path('newDesignFilter', views.newDesign_filter, name="newDesignFilter"),
    path('newdesShow_filter', views.newdesShow_filter, name="newdesShow_filter"),
    path('newDelete/<int:pk>', views.newDelete, name='newDelete'),
    path('nwestshowpageUpdate/<int:pk>', views.nwestshowpageUpdate, name='nwestshowpageUpdate'),
    path('west_sales_stageUpdate/<int:pk>', views.west_sales_stageUpdate, name='west_sales_stageUpdate'),
    path('new_attachments/<int:pk>', views.new_attachments, name='new_attachments'),
    path('nwestdownload/<int:pk>/<int:file_pk>/', views.download_nwestfile, name='nwestdownload'),
    path('nwest_download_all_files/<int:pk>/', views.nwest_download_all_files, name='nwest_download_all_files'),
    path('nwest_to_excel', views.nwest_to_excel, name='nwest_to_excel'),
    path('nwestedit/<int:pk>', views.nwestEdit, name='nwestedit'),
    path('nwestRevisionP/<int:pk>', views.nwestRevisionP, name='nwestRevisionP'),
    path('nwupdate/<int:pk>', views.NWUpdateData, name='nwupdate'),
    path('NWRevInsertData/<int:pk>', views.NWRevInsertData, name="NWRevInsertData"),
    path('nwadd_solutions/<int:pk>', views.nwadd_solutions, name='nwadd_solutions'),
    path('display_nwsolution/<int:pk>', views.display_nwsolution, name='display_nwsolution'),
    path('show_revisions/<int:pk>', views.show_revisions, name='show_revisions'),
    path('dropdown_reason/<int:pk>', views.dropdown_reason, name='dropdown_reason'),

]
