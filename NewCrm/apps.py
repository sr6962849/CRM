from django.apps import AppConfig


class NewcrmConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'NewCrm'
