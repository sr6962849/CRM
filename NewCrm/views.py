import math
from datetime import datetime, time
import io
import mimetypes
import os
import zipfile
from urllib.parse import urlparse

from django.contrib.auth import authenticate, update_session_auth_hash
import pandas as pd
import pytz
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import user_passes_test
from django.db.models import Max
from django.http import FileResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.dateparse import parse_date
from .models import NWest, FileModel, Rcc_FilModel, GroundMount_FilModel, IS_FilModel, NWRevision
from django.contrib import messages
from django.utils import timezone
from NewPs.models import Ps, PSFileModel


# Create your views here.

def change_password(request):
    if request.method == 'POST':
        current_password = request.POST.get('current_password')
        new_password = request.POST.get('new_password')
        confirm_password = request.POST.get('confirm_password')

        if new_password != confirm_password:
            messages.error(request, 'New password and Current password do not match.')
            return redirect('change_password')
        # here first we check if the entered new password and current password are same or not

        user = authenticate(username=request.user.username, password=current_password)
        # then we authenticate current password is correct or not
        # if it is corerect than if block will execute and password will be changed
        # else error message will be printed

        if user is not None:
            user.set_password(new_password)
            user.save()
            # with set_password() method we set the new password
            # and with save() method we save the new password

            update_session_auth_hash(request, user)  # Important!
            #  this method used to keep the user logged in after they change their password.
            #  Without this, the user would be logged out immediately after the password change,
            #  as their session would no longer match their new credentials.

            messages.success(request, 'Your password was successfully updated!')
            return redirect('newDesign')
        else:
            messages.error(request, 'Current password is incorrect.')

    return render(request, 'change_password.html')


def newInsert(request):
    user = request.user
    clients = NWest.objects.all().values_list('client', flat=True).distinct()
    user_groups = request.user.groups.all()
    # Check if the user is in more than one group or is a superuser
    show_region_select = request.user.is_superuser or len(user_groups) > 1

    context = {
        'user': user,
        'clients': list(clients),
        'show_region_select': show_region_select,
    }
    return render(request, 'newInsert.html', context)


def generate_client_id(client_name):
    words = client_name.split()

    # If the client name has three or more words
    if len(words) >= 3:
        client_id = ''.join([word[0].upper() for word in words[:3]])
    # If the client name has two words
    elif len(words) == 2:
        client_id = ''.join([word[0].upper() for word in words])
        if len(client_id) < 3:
            remaining_chars = (words[1][1:] + words[0][1:]).upper()  # Take remaining characters
            client_id += remaining_chars[0] if remaining_chars else ''
    # If the client name has one word
    else:
        word = words[0].upper()
        client_id = word[:3] if len(word) >= 3 else word + 'X' * (3 - len(word))

    # Ensure the client ID is unique
    original_client_id = client_id
    counter = 1
    while NWest.objects.filter(client_id=client_id).exists():
        client_id = f"{original_client_id}{counter}"
        counter += 1

    return client_id


def generate_project_id(client_id):
    # Get the number of projects with the same client_id
    existing_projects = NWest.objects.filter(client_id=client_id)
    project_count = existing_projects.count() + 1
    project_id = f"{client_id}/{project_count:02d}"
    return project_id


def NInsertData(request):
    result = NWest.objects.aggregate(Max('sr'))
    latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
    # maximum id/sr is retrieved from database than it is assigned to latest_sr
    # if it is noth there than 0 is assigned and then later we add 1
    # each time we create row
    sr = latest_sr + 1
    client = request.POST.get('client')

    # Check if the client already exists in the database
    existing_client = NWest.objects.filter(client=client).first()
    if existing_client:
        client_id = existing_client.client_id
    else:
        client_id = generate_client_id(client)

    project_name = request.POST.get('project_name')
    project_location = request.POST.get('project_location', '').strip()

    project_location_l = None
    if project_location == "Domestic":
        project_location_l = request.POST.get('domestic_opt', '').strip()
    elif project_location == "International":
        project_location_l = request.POST.get('international_inp', '').strip()

    # Set default to "Andhra Pradesh" if no valid value is provided
    if not project_location_l:
        project_location_l = "Andhra Pradesh"

    # Generate the project_id based on the client_id
    project_id = generate_project_id(client_id)

    client_requirements = []
    if request.POST.get('mms'):
        client_requirements.append('MMS')
    if request.POST.get('walkway'):
        client_requirements.append('Walkway')
    if request.POST.get('handrail'):
        client_requirements.append('Handrail')
    if request.POST.get('carport'):
        client_requirements.append('Carport')
    if request.POST.get('clientreq_other'):
        clientreq_otherinp = request.POST.get('clientreq_otherinp')
        if clientreq_otherinp:
            client_requirements.append(clientreq_otherinp)

    # Join the requirements into a single string
    client_requirements_str = ' + '.join(client_requirements)

    capacity = request.POST.get('capacity')
    length_walkway = request.POST.get('length_walkway')
    length_handrail = request.POST.get('length_handrail')
    design_intervention = request.POST.get('design_intervention')
    installation_types = []
    if request.POST.get('rcc'):
        installation_types.append('RCC')
    if request.POST.get('groundmount'):
        installation_types.append('Ground Mount')
    if request.POST.get('industrialshed'):
        installation_types.append('Industrial Shed')
    installation_type_str = ' + '.join(installation_types)

    design_request_modes = []
    if request.POST.get('email'):
        design_request_modes.append('Email')
    if request.POST.get('call'):
        design_request_modes.append('Call')
    if request.POST.get('whatsapp'):
        design_request_modes.append('Whatsapp')
    if request.POST.get('designreq_other'):
        designreq_otherinp = request.POST.get('designreq_otherinp')
        if designreq_otherinp:
            design_request_modes.append(designreq_otherinp)
    design_request_mode_str = ' + '.join(design_request_modes)

    requirement_for_design = []
    if request.POST.get('ga'):
        requirement_for_design.append('GA')
    if request.POST.get('costing'):
        requirement_for_design.append('Costing')
    if request.POST.get('bom'):
        requirement_for_design.append('BOM')
    if request.POST.get('fea_staad_report'):
        requirement_for_design.append('FEA/Staad Report')
    if request.POST.get('layout'):
        requirement_for_design.append('Layout')
    if request.POST.get('installation_manual'):
        requirement_for_design.append('Installation Manual')
    if request.POST.get('drawingreq_other'):
        drawingreq_otherinp = request.POST.get('drawingreq_otherinp')
        if drawingreq_otherinp:
            requirement_for_design.append(drawingreq_otherinp)
    requirement_for_design_str = ' + '.join(requirement_for_design)

    remarks = request.POST.get('remarks')
    design_assigned = None
    sales_status = "Pre Sales"
    design_status = "To Start"
    region = request.POST.get('region')
    if region is None:
        if request.user.groups.filter(name='NNorth').exists():
            region = "North"
        elif request.user.groups.filter(name='NWest').exists():
            region = "West"
        elif request.user.groups.filter(name='NSouth').exists():
            region = "South"
        elif request.user.groups.filter(name='NInternational').exists():
            region = "International"
    enquiry_from = request.user.username
    r_user = request.user.username
    # r_user is remark user

    newrow = NWest.objects.create(sr=sr, client=client, client_id=client_id,
                                  project_name=project_name,
                                  project_location=project_location_l,
                                  project_id=project_id,
                                  client_requirements=client_requirements_str,
                                  capacity=capacity, length_walkway=length_walkway,
                                  length_handrail=length_handrail,
                                  design_intervention=design_intervention,
                                  installation_type=installation_type_str,
                                  design_request=design_request_mode_str,
                                  requirement_for_design=requirement_for_design_str,
                                  remarks=remarks, design_assigned=design_assigned,
                                  sales_status=sales_status, design_status=design_status,
                                  region=region, enquiry_from=enquiry_from, r_user=r_user)

    if newrow.crm_entry_date:
        ist_timezone = pytz.timezone('Asia/Kolkata')
        formatted_date = newrow.crm_entry_date.astimezone(ist_timezone).strftime('%d/%m/%y %H:%M')
        # If you need to use `formatted_date` here or pass it to a template, do so.
        NWest.objects.filter(id=newrow.id).update(crm_entry_date_formatted=formatted_date)

    document_files = request.FILES.getlist('document_file')
    user = request.user.username
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date1 = ist_time.strftime('%d/%m/%y %H:%M')
    print(formatted_date1)

    for file in document_files:
        file_instance = FileModel.objects.create(document_file=file, user=user, date_time=formatted_date1)
        newrow.document_files.add(file_instance)
    return redirect('newDesign')


def sales_groups(user):
    # Check if the user belongs to any of the allowed groups
    return user.groups.filter(name__in=["NWest", "NNorth", "NSouth", "NInternational"]).exists() or user.is_superuser


def design_groups(user):
    # Check if the user belongs to design group
    return user.groups.filter(name__in=["NDesign"]).exists() or user.is_superuser


@user_passes_test(sales_groups)
def newDesign(request):
    all_data = None
    region = None
    regions = []  # List to hold the regions for the user

    # Check if the user belongs to each group and add corresponding region to the list
    if request.user.groups.filter(name='NNorth').exists():
        regions.append('North')
    if request.user.groups.filter(name='NWest').exists():
        regions.append('West')
    if request.user.groups.filter(name='NSouth').exists():
        regions.append('South')
    if request.user.groups.filter(name='NInternational').exists():
        regions.append('International')

    # Check if the user is a superuser
    if request.user.is_superuser:
        all_data = NWest.objects.all()
        region = ''
    else:
        # If user belongs to multiple regions, filter by those regions
        if regions:
            all_data = NWest.objects.filter(region__in=regions)  # Fetch data for all regions in the list
            region = ", ".join(regions)  # Create a string to display all user's regions
        else:
            all_data = None  # Handle case where user doesn't belong to any relevant group

    user = request.user
    context = {
        'all_data': all_data,
        'user': user,
        'region': region,

    }
    return render(request, 'newDesign.html', context)


def newDesign_filter(request):
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    filter_v = request.GET.get('filters')
    installation_type = request.GET.getlist('installtion_type_filter')
    sales_status = request.GET.get('sales_status_filter', '')

    if start_date:
        start_date = datetime.combine(parse_date(start_date), time.min)
    if end_date:
        end_date = datetime.combine(parse_date(end_date), time.max)

    queryset = NWest.objects.all()
    queryset_date = None
    queryset_client = None
    queryset_location = None
    queryset_installationtype = None
    queryset_sales_status = None

    if start_date and end_date:
        queryset_date = queryset.filter(crm_entry_date__range=[start_date, end_date])
    elif start_date:
        queryset_date = queryset.filter(crm_entry_date__gte=start_date)
    elif end_date:
        queryset_date = queryset.filter(crm_entry_date__lte=end_date)

    client_name = request.GET.get('client_filter', '')
    if client_name:
        queryset_client = queryset.filter(client__icontains=client_name)

    location = request.GET.get('location_filter', '')
    if location:
        queryset_location = queryset.filter(project_location__icontains=location)

    if installation_type:
        queryset_installationtype = queryset.filter(installation_type__icontains=' + '.join(installation_type))

    if sales_status:
        queryset_sales_status = queryset.filter(sales_status=sales_status)

    if filter_v == "Client Name":
        queryset = queryset_client
    elif filter_v == "Entry Date":
        queryset = queryset_date
    elif filter_v == "Location":
        queryset = queryset_location
    elif filter_v == "Installation Type":
        queryset = queryset_installationtype
    elif filter_v == "Sales Status":
        queryset = queryset_sales_status

    region = None
    if request.user.groups.filter(name='NNorth').exists():
        region = "North"
    if request.user.groups.filter(name='NWest').exists():
        region = "West"
    if request.user.groups.filter(name='NSouth').exists():
        region = "South"
    if request.user.groups.filter(name='NInternational').exists():
        region = "International"
    user = request.user

    context = {
        'filtered_data': queryset,
        'all_data': queryset,
        'region': region,
        'user': user,
        'start_date': request.GET.get('start_date', ''),
        'end_date': request.GET.get('end_date', ''),
    }
    return render(request, 'newDesign_filter.html', context)


def nwestshowpageUpdate(request, pk):
    design_update = NWest.objects.get(sr=pk)
    design_update.design_status = request.POST.get('design_status1')
    reason = request.POST.get('reason')
    design_update.drop_down_reason = reason
    design_update.save()
    return redirect(request.META.get('HTTP_REFERER', '/'))


def west_sales_stageUpdate(request, pk):
    sales_stage_upd = NWest.objects.get(sr=pk)
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date = ist_time.strftime('%d/%m/%y %H:%M')

    sales_status = request.POST.get('sales_status')
    sales_stage_upd.sales_status = sales_status
    if sales_status == "Post Sales":
        sales_stage_upd.post_sales_date = ist_time
        sales_stage_upd.post_sales_date_f = formatted_date

    sales_stage_upd.save()
    if sales_status == "Post Sales":
        # Extract the necessary data from the form
        po_number = request.POST.get('po_reference')
        po_attachments = request.FILES.getlist('po_attachments')
        input_comments = request.POST.get('input_comments')

        # Get the latest 'sr' (serial number) value
        result = Ps.objects.aggregate(Max('sr'))
        latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
        sr = latest_sr + 1

        # Create file instances and save them
        file_instances = []
        if po_attachments:
            for file in po_attachments:
                # Create the FileModel instances and save them
                file_instance = PSFileModel.objects.create(
                    document_file=file,
                    user=request.user.username,
                    upload_datetime=formatted_date
                )
                file_instances.append(file_instance)

        # Create a new PostSales entry and assign files
        newrow = Ps.objects.create(
            sr=sr,
            client_name=sales_stage_upd.client,
            proj_code=sales_stage_upd.project_id,
            po_number=po_number,
            po_date_unf=ist_time,
            po_date=formatted_date,
            input_coments=input_comments,
            delete='No',
        )

        # Associate the saved files with the new row using set()
        newrow.po_file.set(file_instances)

        # Save the new PostSales entry
        newrow.save()

    return redirect('newDesign')


def newDelete(request, pk):
    ddata = NWest.objects.get(sr=pk)
    # deleting the column
    ddata.delete()
    return redirect(request.META.get('HTTP_REFERER', '/'))


def new_attachments(request, pk):
    nwest_data = NWest.objects.filter(sr=pk)
    context = {
        'nwest_data': nwest_data,
    }
    return render(request, 'newAttachment.html', context)


def download_nwestfile(request, pk, file_pk):
    instance = get_object_or_404(NWest, sr=pk)
    file_instance = get_object_or_404(FileModel, id=file_pk)

    response = FileResponse(file_instance.document_file)
    mime_type, _ = mimetypes.guess_type(file_instance.document_file.name)
    response['Content-Type'] = mime_type

    if not (mime_type and (mime_type.startswith('image') or mime_type == 'application/pdf')):
        response['Content-Disposition'] = f'attachment; filename="{file_instance.document_file.name}"'

    return response


def nwest_download_all_files(request, pk):
    instance = get_object_or_404(NWest, sr=pk)
    files = instance.document_files.all()

    # Create a zip file in memory
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zip_file:
        for file_instance in files:
            file_path = file_instance.document_file.path
            file_name = os.path.basename(file_path)
            zip_file.write(file_path, arcname=file_name)

    # Construct response
    zip_buffer.seek(0)
    response = HttpResponse(zip_buffer, content_type='application/zip')

    # Use a different attribute for the zip file name, e.g., 'sr' or any other relevant attribute
    response['Content-Disposition'] = f'attachment; filename="files.zip"'
    return response


def nwest_to_excel(request):
    # Fetch your table data as a queryset
    queryset = NWest.objects.all()

    # Convert the queryset to a Pandas DataFrame
    df = pd.DataFrame.from_records(queryset.values())

    # Check for datetime columns and convert them to timezone-unaware
    datetime_columns = df.select_dtypes(include=['datetime64[ns, UTC]', 'datetime64[ns]']).columns
    for column in datetime_columns:
        df[column] = df[column].apply(lambda x: x.replace(tzinfo=None) if pd.notnull(x) else x)

    # Create a response with an Excel attachment
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="West_table_data.xlsx"'

    # Write the DataFrame to the response
    df.to_excel(response, index=False, engine='openpyxl')

    return response


def nwestEdit(request, pk):
    get_data = NWest.objects.get(sr=pk)
    # we can fetch the selected data/row with the help of primary key
    user_id = request.session.get('user_id')
    # user id is retrieved from session
    user1 = User.objects.get(id=user_id)
    # and from user id, usernamer is retrieved and will be printed there
    clients = NWest.objects.all().values_list('client', flat=True).distinct()

    if get_data.document_files.exists():
        file_available = True
    else:
        file_available = False

    sales_user = None
    if request.user.groups.filter(name__in=["NWest", "NNorth", "NSouth", "NInternational"]).exists():
        sales_user = True
    else:
        sales_user = False

    # it checks if the user is from any of the sales group or not
    disp = False
    # Now check for None or empty string
    if not sales_user:
        if get_data.design_assigned is None or get_data.design_assigned == "" or get_data.design_assigned == "None":
            disp = True
        else:
            disp = False

    context = {
        'get_data': get_data,
        'user': user1,
        'file_available': file_available,
        'sales_user': sales_user,
        'disp': disp,
        'clients': list(clients),
    }
    return render(request, 'newEdit.html', context)


def NWUpdateData(request, pk):
    udata = NWest.objects.get(sr=pk)
    client = request.POST.get('client')

    # Check if the client already exists in the database
    existing_client = NWest.objects.filter(client=client).first()
    if existing_client:
        client_id = existing_client.client_id
    else:
        client_id = generate_client_id(client)

    project_name = request.POST.get('project_name')
    project_location = request.POST.get('project_location', '').strip()

    project_location_l = None
    if project_location == "Domestic":
        project_location_l = request.POST.get('domestic_opt', '').strip()
    elif project_location == "International":
        project_location_l = request.POST.get('international_inp', '').strip()

    # Set default to "Andhra Pradesh" if no valid value is provided
    if not project_location_l:
        project_location_l = "Andhra Pradesh"

    # Generate the project_id based on the client_id
    project_id = generate_project_id(client_id)

    client_requirements = []
    if request.POST.get('mms'):
        client_requirements.append('MMS')
    if request.POST.get('walkway'):
        client_requirements.append('Walkway')
    if request.POST.get('handrail'):
        client_requirements.append('Handrail')
    if request.POST.get('carport'):
        client_requirements.append('Carport')
    if request.POST.get('clientreq_other'):
        clientreq_otherinp = request.POST.get('clientreq_otherinp')
        if clientreq_otherinp:
            client_requirements.append(clientreq_otherinp)

    # Join the requirements into a single string
    client_requirements_str = ' + '.join(client_requirements)

    capacity = request.POST.get('capacity')
    length_walkway = request.POST.get('length_walkway')
    length_handrail = request.POST.get('length_handrail')
    design_intervention = request.POST.get('design_intervention')
    installation_types = []
    if request.POST.get('rcc'):
        installation_types.append('RCC')
    if request.POST.get('groundmount'):
        installation_types.append('Ground Mount')
    if request.POST.get('industrialshed'):
        installation_types.append('Industrial Shed')
    installation_type_str = ' + '.join(installation_types)

    design_request_modes = []
    if request.POST.get('email'):
        design_request_modes.append('Email')
    if request.POST.get('call'):
        design_request_modes.append('Call')
    if request.POST.get('whatsapp'):
        design_request_modes.append('Whatsapp')
    if request.POST.get('designreq_other'):
        designreq_otherinp = request.POST.get('designreq_otherinp')
        if designreq_otherinp:
            design_request_modes.append(designreq_otherinp)
    design_request_mode_str = ' + '.join(design_request_modes)

    requirement_for_design = []
    if request.POST.get('ga'):
        requirement_for_design.append('GA')
    if request.POST.get('costing'):
        requirement_for_design.append('Costing')
    if request.POST.get('bom'):
        requirement_for_design.append('BOM')
    if request.POST.get('fea_staad_report'):
        requirement_for_design.append('FEA/Staad Report')
    if request.POST.get('layout'):
        requirement_for_design.append('Layout')
    if request.POST.get('installation_manual'):
        requirement_for_design.append('Installation Manual')
    if request.POST.get('drawingreq_other'):
        drawingreq_otherinp = request.POST.get('drawingreq_otherinp')
        if drawingreq_otherinp:
            requirement_for_design.append(drawingreq_otherinp)
    requirement_for_design_str = ' + '.join(requirement_for_design)

    remarks = request.POST.get('remarks')

    udata.client = client
    udata.client_id = client_id
    udata.project_name = project_name
    udata.project_location = project_location_l
    udata.project_id = udata.project_id
    udata.client_requirements = client_requirements_str
    udata.capacity = capacity
    udata.length_walkway = length_walkway
    udata.length_handrail = length_handrail
    udata.installation_type = installation_type_str
    udata.design_request = design_request_mode_str
    udata.requirement_for_design = requirement_for_design_str
    new_document_files = request.FILES.getlist('document_file')
    # Check if a new file is being uploaded
    if new_document_files:
        # Create instances of FileModel for each uploaded file
        for file in new_document_files:
            file_instance = FileModel(document_file=file)
            file_instance.save()
            udata.document_files.add(file_instance)
    # code for editing multiple file by GP
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date = ist_time.strftime('%d/%m/%y %H:%M')

    groups = Group.objects.filter(name__in=["NWest", "NNorth", "NSouth", "NInternational"])
    print("Group = ", request.user.groups.all())
    design_assigned_n = request.POST.get('design_assigned')
    if udata.design_intervention == "No" and design_intervention == "Yes":
        udata.crm_entry_date_formatted = formatted_date
        udata.design_status = "To Start"
    udata.design_intervention = design_intervention
    if udata.design_assigned != design_assigned_n:
        if not request.user.groups.filter(name__in=["NWest", "NNorth", "NSouth", "NInternational"]).exists():
            udata.design_assigned = design_assigned_n
            udata.ds_assigned_date = formatted_date
            udata.ds_assigned_by = request.user.username
    if udata.remarks != remarks:
        udata.remarks = remarks
        udata.r_user = request.user.username
    udata.save()

    referrer = request.POST.get('referrer', '/')

    # Parse the referrer URL
    parsed_url = urlparse(referrer)
    path_parts = parsed_url.path.strip('/').split('/')

    # Go back two levels in the path if possible
    if len(path_parts) > 1:
        new_path = '/' + '/'.join(path_parts[:-1]) + '/'
    else:
        new_path = parsed_url.path

    # Reconstruct the URL with query parameters
    redirect_url = new_path
    if parsed_url.query:
        redirect_url += '?' + parsed_url.query
    # above code to go back to previous url by gpt

    return HttpResponseRedirect(redirect_url)


@user_passes_test(design_groups)
def newdesShow(request):
    all_data = NWest.objects.filter(design_intervention="Yes")
    user = request.user
    # Fetch groups with names "West", "North", and "South"
    groups = Group.objects.filter(name__in=["NWest", "NNorth", "NSouth", "NInternational"])

    # Fetch users belonging to these groups
    users_list = User.objects.filter(groups__in=groups).distinct()

    design_admin_group = None
    if user.groups.filter(name__in=["DesignAdmin"]).exists():
        design_admin_group = True
    else:
        design_admin_group = False

    to_start_count = NWest.objects.filter(design_status="To Start", design_intervention="Yes").count()
    wip_count = NWest.objects.filter(design_status="WIP", design_intervention="Yes").count()
    hold_count = NWest.objects.filter(design_status="Hold", design_intervention="Yes").count()
    done_count = NWest.objects.filter(design_status="Done", design_intervention="Yes").count()
    post_sales_count = Ps.objects.all().exclude(design_status="Done").count()

    # Filter entries where sales_status is "Post Sales" and post_sales_date is not null
    oldest_entry = Ps.objects.filter(po_date_unf__isnull=False).exclude(
        design_status="Done").order_by('po_date_unf').first()

    hours_difference = 0
    current_time = None

    if oldest_entry and oldest_entry.po_date_unf:
        # Get the current time
        current_time = timezone.now()

        # Calculate the time difference
        time_difference = current_time - oldest_entry.po_date_unf

        # Convert the time difference to hours
        hours_difference = math.floor(time_difference.total_seconds() / 3600)

        # Round the hours difference for better readability
        # hours_difference = round(hours_difference, 2)
    print("hrs differenecr", hours_difference)
    context = {
        'all_data': all_data,
        'user': user,
        'users_list': users_list,
        'to_start_count': to_start_count,
        'wip_count': wip_count,
        'hold_count': hold_count,
        'done_count': done_count,
        'post_sales_count': post_sales_count,
        'hours_difference': hours_difference,
        'design_admin_group': design_admin_group,
    }
    return render(request, 'newdesShow.html', context)


def nwadd_solutions(request, pk):
    nwest_instance = get_object_or_404(NWest, sr=pk)
    files = {
        'ga_sol': request.FILES.getlist('ga_sol'),
        'costing_sol': request.FILES.getlist('costing_sol'),
        'bom_sol': request.FILES.getlist('bom_sol'),
        'fea_staad_sol': request.FILES.getlist('fea_staad_sol'),
        'layout_sol': request.FILES.getlist('layout_sol'),
        'inst_manual_sol': request.FILES.getlist('inst_manual_sol'),
    }
    gm_files = {
        'ga_sol': request.FILES.getlist('gm_ga_sol'),
        'costing_sol': request.FILES.getlist('gm_costing_sol'),
        'bom_sol': request.FILES.getlist('gm_bom_sol'),
        'fea_staad_sol': request.FILES.getlist('gm_fea_staad_sol'),
        'layout_sol': request.FILES.getlist('gm_layout_sol'),
        'inst_manual_sol': request.FILES.getlist('gm_inst_manual_sol'),
    }
    is_files = {
        'ga_sol': request.FILES.getlist('is_ga_sol'),
        'costing_sol': request.FILES.getlist('is_costing_sol'),
        'bom_sol': request.FILES.getlist('is_bom_sol'),
        'fea_staad_sol': request.FILES.getlist('is_fea_staad_sol'),
        'layout_sol': request.FILES.getlist('is_layout_sol'),
        'inst_manual_sol': request.FILES.getlist('is_inst_manual_sol'),
        'is_other_inp': request.FILES.getlist('is_other_sol'),
    }

    for file_type, file_list in files.items():
        for file in file_list:
            Rcc_FilModel.objects.create(file=file, file_type=file_type, nwest_id=nwest_instance.sr)

    for file_type, file_list in gm_files.items():
        for file in file_list:
            GroundMount_FilModel.objects.create(file=file, file_type=file_type, nwest_id=nwest_instance.sr)

    for file_type, file_list in is_files.items():
        for file in file_list:
            IS_FilModel.objects.create(file=file, file_type=file_type, nwest_id=nwest_instance.sr)

    return redirect('newdesShow')


def display_nwsolution(request, pk):
    nwest_instance = get_object_or_404(NWest, sr=pk)
    files = Rcc_FilModel.objects.filter(nwest_id=nwest_instance.sr)
    gm_files = GroundMount_FilModel.objects.filter(nwest_id=nwest_instance.sr)
    is_files = IS_FilModel.objects.filter(nwest_id=nwest_instance.sr)

    # Group files by their type
    file_groups = {
        'ga_sol': [],
        'costing_sol': [],
        'bom_sol': [],
        'fea_staad_sol': [],
        'layout_sol': [],
        'inst_manual_sol': []
    }
    gm_file_groups = {
        'ga_sol': [],
        'costing_sol': [],
        'bom_sol': [],
        'fea_staad_sol': [],
        'layout_sol': [],
        'inst_manual_sol': []
    }
    is_file_groups = {
        'ga_sol': [],
        'costing_sol': [],
        'bom_sol': [],
        'fea_staad_sol': [],
        'layout_sol': [],
        'inst_manual_sol': [],
        'is_other_inp': [],
    }

    for file in files:
        file_groups[file.file_type].append(file)

    for file in gm_files:
        gm_file_groups[file.file_type].append(file)

    for file in is_files:
        is_file_groups[file.file_type].append(file)

    context = {
        'file_groups': file_groups,
        'gm_file_groups': gm_file_groups,
        'is_file_groups': is_file_groups,
        'nwest_instance': nwest_instance,
    }
    return render(request, 'show_solutions.html', context)


def get_next_revision_number(sr):
    # Get all revisions for the given sr and extract the revision numbers
    existing_revisions = NWRevision.objects.filter(sr=sr).values_list('revision_number', flat=True)
    revision_numbers = [int(rev[1:]) for rev in existing_revisions if rev.startswith('R')]

    # Determine the next revision number
    next_revision_number = max(revision_numbers, default=0) + 1
    return f'R{next_revision_number}'


def nwestRevisionP(request, pk):
    get_data = NWest.objects.get(sr=pk)
    # we can fetch the selected data/row with the help of primary key
    user_id = request.session.get('user_id')
    # user id is retrieved from session
    user1 = User.objects.get(id=user_id)
    # and from user id, usernamer is retrieved and will be printed there
    clients = NWest.objects.all().values_list('client', flat=True).distinct()
    context = {
        'get_data': get_data,
        'user': user1,
        'clients': list(clients),
    }
    return render(request, 'Revision.html', context)


def NWRevInsertData(request, pk):
    next_revision_number = get_next_revision_number(pk)
    nwest_instance = NWest.objects.get(sr=pk)
    sr = pk
    client = request.POST.get('client')

    # Check if the client already exists in the database
    existing_client = NWest.objects.filter(client=client).first()
    if existing_client:
        client_id = existing_client.client_id
    else:
        client_id = generate_client_id(client)

    project_name = request.POST.get('project_name')
    project_location = request.POST.get('project_location', '').strip()

    project_location_l = None
    if project_location == "Domestic":
        project_location_l = request.POST.get('domestic_opt', '').strip()
    elif project_location == "International":
        project_location_l = request.POST.get('international_inp', '').strip()

    # Set default to "Andhra Pradesh" if no valid value is provided
    if not project_location_l:
        project_location_l = "Andhra Pradesh"

    # Generate the project_id based on the client_id
    project_id = generate_project_id(client_id)

    client_requirements = []
    if request.POST.get('mms'):
        client_requirements.append('MMS')
    if request.POST.get('walkway'):
        client_requirements.append('Walkway')
    if request.POST.get('handrail'):
        client_requirements.append('Handrail')
    if request.POST.get('carport'):
        client_requirements.append('Carport')
    if request.POST.get('clientreq_other'):
        clientreq_otherinp = request.POST.get('clientreq_otherinp')
        if clientreq_otherinp:
            client_requirements.append(clientreq_otherinp)

    # Join the requirements into a single string
    client_requirements_str = ' + '.join(client_requirements)

    capacity = request.POST.get('capacity')
    length_walkway = request.POST.get('length_walkway')
    length_handrail = request.POST.get('length_handrail')
    design_intervention = request.POST.get('design_intervention')
    installation_types = []
    if request.POST.get('rcc'):
        installation_types.append('RCC')
    if request.POST.get('groundmount'):
        installation_types.append('Ground Mount')
    if request.POST.get('industrialshed'):
        installation_types.append('Industrial Shed')
    installation_type_str = ' + '.join(installation_types)

    design_request_modes = []
    if request.POST.get('email'):
        design_request_modes.append('Email')
    if request.POST.get('call'):
        design_request_modes.append('Call')
    if request.POST.get('whatsapp'):
        design_request_modes.append('Whatsapp')
    if request.POST.get('designreq_other'):
        designreq_otherinp = request.POST.get('designreq_otherinp')
        if designreq_otherinp:
            design_request_modes.append(designreq_otherinp)
    design_request_mode_str = ' + '.join(design_request_modes)

    requirement_for_design = []
    if request.POST.get('ga'):
        requirement_for_design.append('GA')
    if request.POST.get('costing'):
        requirement_for_design.append('Costing')
    if request.POST.get('bom'):
        requirement_for_design.append('BOM')
    if request.POST.get('fea_staad_report'):
        requirement_for_design.append('FEA/Staad Report')
    if request.POST.get('layout'):
        requirement_for_design.append('Layout')
    if request.POST.get('installation_manual'):
        requirement_for_design.append('Installation Manual')
    if request.POST.get('drawingreq_other'):
        drawingreq_otherinp = request.POST.get('drawingreq_otherinp')
        if drawingreq_otherinp:
            requirement_for_design.append(drawingreq_otherinp)
    requirement_for_design_str = ' + '.join(requirement_for_design)

    remarks = request.POST.get('remarks')
    design_assigned = request.user
    region = nwest_instance.region
    enquiry_from = nwest_instance.enquiry_from
    design_status = nwest_instance.design_status

    newrow = NWRevision.objects.create(sr=sr, client=client, client_id=client_id,
                                       project_name=project_name,
                                       project_location=project_location_l,
                                       project_id=project_id,
                                       client_requirements=client_requirements_str,
                                       capacity=capacity, length_walkway=length_walkway,
                                       length_handrail=length_handrail,
                                       design_intervention=design_intervention,
                                       installation_type=installation_type_str,
                                       design_request=design_request_mode_str,
                                       requirement_for_design=requirement_for_design_str,
                                       remarks=remarks, design_assigned=design_assigned,
                                       revision_number=next_revision_number,
                                       region=region, enquiry_from=enquiry_from,
                                       design_status=design_status)

    if newrow.crm_entry_date:
        ist_timezone = pytz.timezone('Asia/Kolkata')
        formatted_date = newrow.crm_entry_date.astimezone(ist_timezone).strftime('%d/%m/%y %H:%M')
        # If you need to use `formatted_date` here or pass it to a template, do so.
        NWRevision.objects.filter(id=newrow.id).update(crm_entry_date_formatted=formatted_date)

    return redirect('newdesShow')


def show_revisions(request, pk):
    all_data = NWRevision.objects.filter(sr=pk).order_by('revision_number')
    single_data = NWRevision.objects.filter(sr=pk).first()
    user = request.user
    context = {
        'all_data': all_data,
        'single_data': single_data,
        'user': user,
    }
    return render(request, 'Show_Revision.html', context)


def newdesShow_filter(request):
    user = request.user
    design_status = request.GET.get('design_status_filter')
    design_status_b = request.GET.get('design_status_b')
    region = request.GET.get('region_filter')
    enquiry_from = request.GET.get('enquiry_from_filter')
    sales_status = request.GET.get('sales_status_filter', '')

    queryset = None

    if design_status:
        queryset = NWest.objects.filter(design_status=design_status, design_intervention="Yes")

    elif design_status_b:
        queryset = NWest.objects.filter(design_status=design_status_b, design_intervention="Yes")

    elif region:
        if region == "West":
            queryset = NWest.objects.filter(region="West", design_intervention="Yes")
        elif region == "North":
            queryset = NWest.objects.filter(region="North", design_intervention="Yes")
        elif region == "South":
            queryset = NWest.objects.filter(region="South", design_intervention="Yes")
        elif region == "International":
            queryset = NWest.objects.filter(region="International", design_intervention="Yes")

    elif enquiry_from:
        queryset = NWest.objects.filter(enquiry_from=enquiry_from, design_intervention="Yes")

    elif sales_status:
        queryset = NWest.objects.filter(sales_status=sales_status, design_intervention="Yes")

    to_start_count = NWest.objects.filter(design_status="To Start", design_intervention="Yes").count()
    wip_count = NWest.objects.filter(design_status="WIP", design_intervention="Yes").count()
    hold_count = NWest.objects.filter(design_status="Hold", design_intervention="Yes").count()
    done_count = NWest.objects.filter(design_status="Done", design_intervention="Yes").count()

    context = {
        'filtered_data': queryset,
        'user': user,
        'to_start_count': to_start_count,
        'wip_count': wip_count,
        'hold_count': hold_count,
        'done_count': done_count,
    }
    return render(request, 'newdesShow_filter.html', context)


def dropdown_reason(request, pk):
    udata = NWest.objects.get(sr=pk)
    reason = request.POST.get('reason')
    print("Reason = ", reason)
    udata.drop_down_reason = reason
    udata.save()
    return redirect('newdesShow')
