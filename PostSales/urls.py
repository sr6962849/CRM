from django.urls import path
from . import views

urlpatterns = [
    path('ps', views.PostSales1, name="ps1"),
    path('Pop/<int:pk>', views.PopupForm, name="pop"),
    path('PopN/<int:pk>', views.PopupFormN, name="popN"),
    path('PopS/<int:pk>', views.PopupFormS, name="popS"),
    path('PopI/<int:pk>', views.PopupFormI, name="popI"),
    path('PsEdit/<int:pk>', views.PsEditpage, name="PsEdit"),
    path('PsUpdate/<int:pk>', views.PsUpdateData, name="PsUpdate"),
    path('PsDelete/<int:pk>', views.PsDelete, name="PsDelete"),
    path('PsAddRevision/<int:pk>', views.add_revision, name="PsAddRevision"),
    path('ps_attachment/<int:pk>', views.view_attachments, name="ps_attachment"),
    path('download_attachment/<int:pk>/<int:file_pk>', views.download_attachment, name="download_attachment"),
    path('bom_delete_file/<int:column_id>/<int:file_id>/', views.bom_delete_file, name='bom_delete_file'),
    path('drawing_delete_file/<int:column_id>/<int:file_id>/', views.drawing_delete_file, name='drawing_delete_file'),
    path('other_delete_file/<int:column_id>/<int:file_id>/', views.other_delete_file, name='other_delete_file'),
]
