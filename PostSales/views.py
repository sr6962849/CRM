import os
from django.conf import settings
from django.contrib.auth.models import User
from django.http import FileResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from Status.models import Column, North, South
from International.models import International
from .models import PostSales, FileModel
from django.db.models import Max
from datetime import datetime


# Create your views here.


def PopupForm(request, pk):
    if request.method == 'POST':
        order_reference = request.POST.get('order_ref')
        order_date = request.POST.get('order_date')

        result = PostSales.objects.aggregate(Max('sr'))
        latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
        # maximum id/sr is retrieved from database than it is assigned to latest_sr
        # if it is noth there than 0 is assigned and then later we add 1
        # each time we create row

        get_presales = Column.objects.get(id=pk)
        sr = latest_sr + 1
        revision_number = sr * 1000
        client_name = get_presales.client
        project_name = get_presales.project_name
        project_code = get_presales.job_no
        types_of_material = get_presales.installation_type
        get_presales.sales_stage = 'Post Sales'
        get_presales.save()

        newrow = PostSales.objects.create(sr=sr, client_name=client_name,
                                          project_code=project_code,
                                          revision_number=revision_number,
                                          order_refrence=order_reference,
                                          po_date=order_date, project_name=project_name,
                                          types_of_material=types_of_material)

        return redirect('ps1')
    # we created PopupForm View to fetch the data of ou Popup form
    else:
        return render(request, 'edit.html')


def PopupFormN(request, pk):
    if request.method == 'POST':
        order_reference = request.POST.get('order_ref')
        order_date = request.POST.get('order_date')

        result = PostSales.objects.aggregate(Max('sr'))
        latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
        # maximum id/sr is retrieved from database than it is assigned to latest_sr
        # if it is noth there than 0 is assigned and then later we add 1
        # each time we create row

        get_presales = North.objects.get(id=pk)
        sr = latest_sr + 1
        revision_number = sr * 1000
        client_name = get_presales.client
        project_name = get_presales.project_name
        project_code = get_presales.job_no
        types_of_material = get_presales.installation_type

        newrow = PostSales.objects.create(sr=sr, client_name=client_name,
                                          project_code=project_code,
                                          revision_number=revision_number,
                                          order_refrence=order_reference,
                                          po_date=order_date, project_name=project_name,
                                          types_of_material=types_of_material)

        return redirect('ps1')
    # we created PopupForm View to fetch the data of ou Popup form
    else:
        return render(request, 'northEdit.html')


def PopupFormS(request, pk):
    if request.method == 'POST':
        order_reference = request.POST.get('order_ref')
        order_date = request.POST.get('order_date')

        result = PostSales.objects.aggregate(Max('sr'))
        latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
        # maximum id/sr is retrieved from database than it is assigned to latest_sr
        # if it is noth there than 0 is assigned and then later we add 1
        # each time we create row

        get_presales = South.objects.get(id=pk)
        sr = latest_sr + 1
        revision_number = sr * 1000
        client_name = get_presales.client
        project_name = get_presales.project_name
        project_code = get_presales.job_no
        types_of_material = get_presales.installation_type

        newrow = PostSales.objects.create(sr=sr, client_name=client_name,
                                          project_code=project_code,
                                          revision_number=revision_number,
                                          order_refrence=order_reference,
                                          po_date=order_date, project_name=project_name,
                                          types_of_material=types_of_material)

        return redirect('ps1')
    # we created PopupForm View to fetch the data of ou Popup form
    else:
        return render(request, 'southEdit.html')


def PopupFormI(request, pk):
    if request.method == 'POST':
        order_reference = request.POST.get('order_ref')
        order_date = request.POST.get('order_date')

        result = PostSales.objects.aggregate(Max('sr'))
        latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
        # maximum id/sr is retrieved from database than it is assigned to latest_sr
        # if it is noth there than 0 is assigned and then later we add 1
        # each time we create row

        get_presales = International.objects.get(id=pk)
        sr = latest_sr + 1
        revision_number = sr * 1000
        client_name = get_presales.client
        project_name = get_presales.project_name
        project_code = get_presales.job_no
        types_of_material = get_presales.installation_type

        newrow = PostSales.objects.create(sr=sr, client_name=client_name,
                                          project_code=project_code,
                                          revision_number=revision_number,
                                          order_refrence=order_reference,
                                          po_date=order_date, project_name=project_name,
                                          types_of_material=types_of_material)

        return redirect('ps1')
    # we created PopupForm View to fetch the data of ou Popup form
    else:
        return render(request, 'interEdit.html')


def PostSales1(request):
    postsales_data = PostSales.objects.all().order_by('sr')

    user_id = request.session.get('user_id')
    # user id is retrieved from session
    user = User.objects.get(id=user_id)
    # and from user id, username will retrieved

    editvar = False
    if user.is_superuser or user.groups.filter(name__in=['Designer', 'Production']).exists():
        editvar = True

    context = {
        'postsales_data': postsales_data,
        'user': user,
        'editvar': editvar,
        # other context variables
    }
    return render(request, 'post_sales.html', context)


def PsEditpage(request, pk):
    get_data = PostSales.objects.filter(revision_number=pk).first()

    user_id = request.session.get('user_id')
    # user id is retrieved from session
    user = User.objects.get(id=user_id)
    # and from user id, username will retrieved

    productionvar = False

    if user.is_superuser or user.groups.filter(name='Designer').exists():
        productionvar = True

    context = {
        'get_data': get_data,
        'productionvar': productionvar,
    }
    return render(request, 'ps_edit.html', context)


def PsDelete(request, pk):
    ddata = PostSales.objects.filter(revision_number=pk)
    ddata.delete()
    return redirect('ps1')


def PsUpdateData(request, pk):
    udata = PostSales.objects.get(revision_number=pk)
    # we fetch the row that need to be updated with the help of 'primary key'

    udata.sr = request.POST.get('sr')
    udata.client_name = request.POST.get('client_name')
    udata.project_name = request.POST.get('project_name')
    udata.project_code = request.POST.get('project_code')
    udata.order_refrence = request.POST.get('order_refrence')
    udata.po_date = request.POST.get('po_date')
    udata.types_of_material = request.POST.get('types_of_material')
    udata.reason_for_revision = request.POST.get('reason_for_revision')
    udata.whether_all_input_available = request.POST.get('whether_all_input_available')
    udata.design_status = request.POST.get('design_status')
    udata.maker = request.POST.get('maker')
    udata.checker = request.POST.get('checker')
    udata.bom_release_date = request.POST.get('bom_release_date')
    udata.drawing_release_date = request.POST.get('drawing_release_date')
    udata.other_document_release_date = request.POST.get('other_document_release_date')
    udata.remarks = request.POST.get('remarks')
    udata.turn_around_time = request.POST.get('turn_around_time')

    user_id = request.session.get('user_id')
    # user id is retrieved from session
    user1 = User.objects.get(id=user_id)
    # and from user id, username is retrieved and will be printed there

    bom_attachment = request.FILES.getlist('bom_attachment')

    if bom_attachment:
        for file in bom_attachment:
            file_instance = FileModel(document_file=file, user=user1)
            file_instance.save()
            udata.bom_attachment.add(file_instance)

    drawing_attachment = request.FILES.getlist('drawing_attachment')

    if drawing_attachment:
        for file in drawing_attachment:
            file_instance = FileModel(document_file=file, user=user1)
            file_instance.save()
            udata.drawing_attachment.add(file_instance)

    others_attachment = request.FILES.getlist('others_attachment')

    if others_attachment:
        for file in others_attachment:
            file_instance = FileModel(document_file=file, user=user1)
            file_instance.save()
            udata.other_attachment.add(file_instance)
    # Retrieve the last uploaded file instance from the ManyToManyField
    latest_bom_attachment = udata.bom_attachment.order_by('-upload_datetime').first()

    # Check if there's a latest uploaded file
    if latest_bom_attachment:
        # Extract the upload date from the last uploaded file instance
        bom_release_date = latest_bom_attachment.upload_datetime.strftime('%Y-%m-%d')

        # Save the upload date in the bom_release_date field
        udata.bom_release_date = bom_release_date

    latest_drawing_attachment = udata.drawing_attachment.order_by('-upload_datetime').first()

    if latest_drawing_attachment:
        drawing_release_date = latest_drawing_attachment.upload_datetime.strftime('%Y-%m-%d')

        udata.drawing_release_date = drawing_release_date

    latest_other_attachment = udata.other_attachment.order_by('-upload_datetime').first()

    if latest_other_attachment:
        other_release_date = latest_other_attachment.upload_datetime.strftime('%Y-%m-%d')

        udata.other_document_release_date = other_release_date

    po_date_str = udata.po_date
    bom_release_date_str = udata.bom_release_date

    # Check if both date strings are not empty
    if po_date_str and bom_release_date_str:
        # Parse the dates into datetime objects
        po_date_obj = datetime.strptime(po_date_str, '%Y-%m-%d')
        bom_release_date_obj = datetime.strptime(bom_release_date_str, '%Y-%m-%d')

        # Calculate the difference in days
        turnaround_time_days = (bom_release_date_obj - po_date_obj).days
        udata.turn_around_time = str(turnaround_time_days) + ' day(s)'
    else:
        # Handle the case where one or both date strings are empty
        # For example, you can set udata.turn_around_time to some default value or handle it accordingly
        udata.turn_around_time = "N/A"  # Or any other appropriate value

    udata.save()
    return redirect('ps1')


def add_revision(request, pk):
    rdata = PostSales.objects.get(revision_number=pk)
    count_of_rows = PostSales.objects.filter(sr=rdata.sr).count()
    display_revision = PostSales.objects.filter(sr=rdata.sr).first()

    if display_revision.revision != '' and display_revision.revision is not None:
        display_revision.revision = int(display_revision.revision) + 1
    else:
        display_revision.revision = 1
    display_revision.save()

    sr = rdata.sr
    revision_number = rdata.revision_number + count_of_rows
    client_name = rdata.client_name
    project_name = rdata.project_name
    project_code = rdata.project_code

    reviserow = PostSales.objects.create(sr=sr, revision_number=revision_number,
                                         client_name=client_name,
                                         project_name=project_name,
                                         project_code=project_code)
    return redirect('ps1')


def view_attachments(request, pk):
    attachment_data = PostSales.objects.filter(revision_number=pk)
    return render(request, 'ps_attachment.html', {'attachment_data': attachment_data})


def download_attachment(request, pk, file_pk):
    instance = get_object_or_404(PostSales, revision_number=pk)
    file_instance = get_object_or_404(FileModel, id=file_pk)

    response = FileResponse(file_instance.document_file)
    response['Content-Disposition'] = f'attachment; filename="{file_instance.document_file.name}"'
    return response


def bom_delete_file(request, column_id, file_id):
    try:
        column = PostSales.objects.get(revision_number=column_id)
        file_instance = column.bom_attachment.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except Column.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


def drawing_delete_file(request, column_id, file_id):
    try:
        column = PostSales.objects.get(revision_number=column_id)
        file_instance = column.drawing_attachment.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except Column.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


def other_delete_file(request, column_id, file_id):
    try:
        column = PostSales.objects.get(revision_number=column_id)
        file_instance = column.other_attachment.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except Column.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)
