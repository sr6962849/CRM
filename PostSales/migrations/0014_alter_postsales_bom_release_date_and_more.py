# Generated by Django 4.2.3 on 2024-05-14 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PostSales', '0013_alter_filemodel_upload_datetime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postsales',
            name='bom_release_date',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='postsales',
            name='drawing_release_date',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='postsales',
            name='other_document_release_date',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='postsales',
            name='remarks',
            field=models.CharField(blank=True, max_length=2000, null=True),
        ),
        migrations.AlterField(
            model_name='postsales',
            name='turn_around_time',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
