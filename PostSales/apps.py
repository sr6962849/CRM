from django.apps import AppConfig


class PostsalesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PostSales'
