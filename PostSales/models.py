from django.db import models


# Create your models here.

class FileModel(models.Model):
    user = models.CharField(max_length=100, blank=True, null=True)
    document_file = models.FileField(upload_to='')
    upload_datetime = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class PostSales(models.Model):
    sr = models.IntegerField()
    revision_number = models.IntegerField()
    client_name = models.CharField(max_length=100, blank=True, null=True)
    project_name = models.CharField(max_length=100, blank=True, null=True)
    project_code = models.CharField(max_length=100, blank=True, null=True)
    order_refrence = models.CharField(max_length=200, blank=True, null=True)
    po_date = models.CharField(max_length=30, blank=True, null=True)
    types_of_material = models.CharField(max_length=100, blank=True, null=True)
    revision = models.CharField(max_length=50, blank=True, null=True)
    reason_for_revision = models.CharField(max_length=100, blank=True, null=True)
    whether_all_input_available = models.CharField(max_length=100, blank=True, null=True)
    design_status = models.CharField(max_length=50, blank=True, null=True)
    maker = models.CharField(max_length=100, blank=True, null=True)
    checker = models.CharField(max_length=100, blank=True, null=True)
    bom_release_date = models.CharField(max_length=30, blank=True, null=True)
    drawing_release_date = models.CharField(max_length=30, blank=True, null=True)
    other_document_release_date = models.CharField(max_length=30, blank=True, null=True)
    remarks = models.CharField(max_length=2000, blank=True, null=True)
    turn_around_time = models.CharField(max_length=100, blank=True, null=True)
    bom_attachment = models.ManyToManyField(FileModel, related_name='bom_attachments')
    drawing_attachment = models.ManyToManyField(FileModel, related_name='drawing_attachments')
    other_attachment = models.ManyToManyField(FileModel, related_name='other_attachments')

    def __str__(self):
        return f"{self.sr} - {self.project_name}"
