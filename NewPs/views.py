import os
import zipfile
from itertools import chain

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_list_or_404
import pytz
from django.shortcuts import render, redirect
from django.utils import timezone
from .models import Ps, ds_output, revise_ds_output, DesignFlag


# Create your views here.

def dispPs(request):
    all_data = Ps.objects.all()
    user = request.user
    design_and_admin = None
    if user.groups.filter(name__in=["NDesign"]).exists() or user.is_superuser:
        design_and_admin = True
    else:
        design_and_admin = False

    production_and_admin = None
    if user.groups.filter(name__in=["NProduction"]).exists() or user.is_superuser:
        production_and_admin = True
    else:
        production_and_admin = False

    sales_and_admin = None
    if user.groups.filter(name__in=["NWest", "NNorth", "NSouth", "NInternational"]).exists() or user.is_superuser:
        sales_and_admin = True
    else:
        sales_and_admin = False

    context = {
        'all_data': all_data,
        'design_and_admin': design_and_admin,
        'sales_and_admin': sales_and_admin,
        'production_and_admin': production_and_admin,
    }
    return render(request, 'newPs.html', context)


def design_flag(request, pk):
    design_flag = request.POST.get('design_flag')
    design_hault_reason = request.POST.get('design_hault_reason')
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date = ist_time.strftime('%d/%m/%y %H:%M')

    ps_instance = Ps.objects.get(sr=pk)
    if design_flag == 'Yes':
        ps_instance.design_flag = 'Yes'
        # ps_instance.design_flag_reason = design_hault_reason
        # ps_instance.design_flag_raised_by = request.user.username
        # ps_instance.design_flag_raised_time = formatted_date
        flag = DesignFlag.objects.create(
            design_flag_reason=design_hault_reason,
            design_flag_raised_by=request.user.username,
            design_flag_raised_time=formatted_date,

        )
        ps_instance.design_flag_entry.add(flag)
    elif design_flag == 'No':
        ps_instance.design_flag = 'No'
        ps_instance.design_flag_reason = ''
    ps_instance.save()

    return redirect(request.META.get('HTTP_REFERER', '/'))


def production_flag(request, pk):
    productin_flag = request.POST.get('productin_flag')
    production_hault_reason = request.POST.get('production_hault_reason')
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date = ist_time.strftime('%d/%m/%y %H:%M')

    ps_instance = Ps.objects.get(sr=pk)
    if productin_flag == 'Yes':
        ps_instance.production_flag = 'Yes'
        ps_instance.production_flag_reason = production_hault_reason
        ps_instance.production_flag_raised_by = request.user.username
        ps_instance.production_flag_raised_time = formatted_date
    elif productin_flag == 'No':
        ps_instance.production_flag = 'No'
        ps_instance.production_flag_reason = ''
    ps_instance.save()

    return redirect(request.META.get('HTTP_REFERER', '/'))


def delete_ps(request, pk):
    ddata = Ps.objects.get(sr=pk)
    ddata.delete = 'Yes'
    ddata.save()
    return redirect(request.META.get('HTTP_REFERER', '/'))


def ps_design_status(request, pk):
    ps_instance = Ps.objects.get(sr=pk)
    ps_instance.design_status = request.POST.get('design_status1')
    ps_instance.save()
    return redirect(request.META.get('HTTP_REFERER', '/'))


def design_output(request, pk):
    sr = pk
    all_data = ds_output.objects.filter(sr=pk)

    revise_data = revise_ds_output.objects.filter(sr=pk)
    po_data = Ps.objects.get(sr=pk)

    user = request.user
    design_and_admin = None
    if user.groups.filter(name__in=["NDesign"]).exists() or user.is_superuser:
        design_and_admin = True
    else:
        design_and_admin = False
    counter = 0
    context = {
        'all_data': all_data,
        'revise_data': revise_data,
        'po_data': po_data,
        'sr': sr,
        'design_and_admin': design_and_admin,
        'counter': counter,
    }
    return render(request, 'ps_design_output.html', context)


# def add_design_output(request, pk):
#     ps_instance = Ps.objects.get(sr=pk)
#     ist_timezone = pytz.timezone('Asia/Kolkata')
#     ist_time = timezone.now().astimezone(ist_timezone)
#     formatted_date = ist_time.strftime('%d/%m/%y %H:%M')
#
#     ps_instance.maker = request.POST.get('maker')
#     ps_instance.checker = request.POST.get('checker')
#     ps_instance.op_date_time = formatted_date
#     user = request.user.username
#
#     # Save the instance before adding files if it's a ManyToManyField
#     ps_instance.save()
#
#     design_files = request.FILES.getlist('design_file')
#     for file in design_files:
#         file_instance = PSFileModel.objects.create(
#             document_file=file,
#             user=user,
#             upload_datetime=formatted_date  # Store actual datetime, not formatted string
#         )
#         ps_instance.files.add(file_instance)  # Add the related file
#
#     return redirect(request.META.get('HTTP_REFERER', '/'))

def add_design_output(request, pk, sr_id=None):
    sr = pk
    ds_instance = 1
    if sr_id is not None:
        ds_instance = ds_output.objects.get(sr=sr, sr_id=sr_id)
    sr_id1 = ds_output.objects.filter(sr=sr).count() + 1
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date = ist_time.strftime('%d/%m/%y %H:%M')
    maker = request.POST.get('maker')
    checker = request.POST.get('checker')
    op_date_time = formatted_date
    user = request.user.username
    design_file = request.FILES.get('design_file')
    if design_file:
        files = design_file  # Assign file to FileField

    action = request.POST.get("action")  # Get which button was clicked
    if action == "add":
        new = ds_output.objects.create(sr=sr, sr_id=sr_id1, maker=maker, checker=checker, op_date_time=op_date_time,
                                       user=user, files=design_file, type='Normal')

    elif action == "revise":
        if ds_instance.rev_no is None:
            rev_no = 1
        else:
            rev_no = ds_instance.rev_no + 1
        new = ds_output.objects.create(sr=sr, sr_id=sr_id1, rev_no=rev_no, maker=maker, checker=checker,
                                       op_date_time=op_date_time,
                                       user=user, files=design_file, type='Revise')
        ds_instance.strike = 'Yes'
        ds_instance.save()

    return redirect(request.META.get('HTTP_REFERER', '/'))


def add_revise_design_output(request, pk, sr_id=None, rev=None):
    sr = pk
    revise_ds_instance = 1
    old_revise_instance = None
    if sr_id is not None:
        revise_ds_instance = revise_ds_output.objects.filter(sr=sr, sr_id=sr_id).count()
    if rev is not None:
        old_revise_instance = revise_ds_output.objects.get(sr=sr, sr_id=sr_id, rev_no=rev)
    ds_instance = ds_output.objects.get(sr=sr, sr_id=sr_id)
    rev_no = revise_ds_instance + 1
    ist_timezone = pytz.timezone('Asia/Kolkata')
    ist_time = timezone.now().astimezone(ist_timezone)
    formatted_date = ist_time.strftime('%d/%m/%y %H:%M')
    maker = request.POST.get('maker')
    checker = request.POST.get('checker')
    op_date_time = formatted_date
    user = request.user.username
    design_file = request.FILES.get('design_file')
    if design_file:
        files = design_file  # Assign file to FileField

    action = request.POST.get("action")  # Get which button was clicked

    if action == "revise":
        new = revise_ds_output.objects.create(sr=sr, sr_id=sr_id, rev_no=rev_no, maker=maker, checker=checker,
                                              op_date_time=op_date_time,
                                              user=user, files=design_file)
        old_revise_instance.strike = 'Yes'
        old_revise_instance.save()

    elif action == "orgrevise":
        new = revise_ds_output.objects.create(sr=sr, sr_id=sr_id, rev_no=rev_no, maker=maker, checker=checker,
                                              op_date_time=op_date_time,
                                              user=user, files=design_file)
        ds_instance.strike = 'Yes'
        ds_instance.save()

    return redirect(request.META.get('HTTP_REFERER', '/'))


def download_files(request, sr):
    """Download files based on the button clicked (all or partial)."""
    download_type = request.POST.get('download_type', 'all_files')  # Get button value

    if download_type == "all_files":
        entries_ds = ds_output.objects.filter(sr=sr)  # Download all files
        entries_revise_ds = revise_ds_output.objects.filter(sr=sr)  # Download all files

        # Combine both querysets
        all_entries = chain(entries_ds, entries_revise_ds)
        zip_filename = f"all_files_sr_{sr}.zip"
    else:
        entries_ds = ds_output.objects.filter(sr=sr).exclude(strike="Yes")  # Exclude `strike="Yes"`
        entries_revise_ds = revise_ds_output.objects.filter(sr=sr).exclude(strike="Yes")  # Exclude `strike="Yes"`

        # Combine both querysets
        all_entries = chain(entries_ds, entries_revise_ds)

        zip_filename = f"revised_files_sr_{sr}.zip"

    files = [entry.files.path for entry in all_entries if entry.files]

    if not files:
        return HttpResponse("No valid files available for download.", status=404)

    # Create ZIP file
    zip_path = os.path.join(settings.MEDIA_ROOT, zip_filename)
    with zipfile.ZipFile(zip_path, 'w') as zip_file:
        for file_path in files:
            if os.path.exists(file_path):
                zip_file.write(file_path, os.path.basename(file_path))  # Add to zip

    # Serve the zip file
    with open(zip_path, 'rb') as f:
        response = HttpResponse(f.read(), content_type='application/zip')
        response['Content-Disposition'] = f'attachment; filename="{zip_filename}"'

    # Delete zip after sending response
    os.remove(zip_path)
    return response
