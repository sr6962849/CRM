from django.urls import path

from . import views

urlpatterns = [
    path('newPs', views.dispPs, name="newPs"),
    path('ds_flag/<int:pk>', views.design_flag, name="ds_flag"),
    path('pd_flag/<int:pk>', views.production_flag, name="pd_flag"),
    path('delete_ps/<int:pk>', views.delete_ps, name="delete_ps"),
    path('ps_design_status/<int:pk>', views.ps_design_status, name="ps_design_status"),
    path('add_design_output/<int:pk>/<int:sr_id>', views.add_design_output, name="add_design_output"),
    path('add_revise_design_output/<int:pk>/<int:sr_id>', views.add_revise_design_output,
         name="add_revise_design_output"),
    path('add_revno_design_output/<int:pk>/<int:sr_id>/<int:rev>', views.add_revise_design_output,
         name="add_revno_design_output"),
    path('add_design_output/<int:pk>', views.add_design_output, name="add_design_output_wo"),
    path('design_output/<int:pk>', views.design_output, name="design_output"),
    path("download_files/<int:sr>/", views.download_files, name="download_files"),
]
