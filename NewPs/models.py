from django.db import models


# Create your models here.
class PSFileModel(models.Model):
    user = models.CharField(max_length=100, blank=True, null=True)
    document_file = models.FileField(upload_to='')
    upload_datetime = models.CharField(max_length=100, blank=True, null=True)


class Ps(models.Model):
    sr = models.IntegerField()
    revision_number = models.IntegerField(blank=True, null=True)
    client_name = models.CharField(max_length=100, blank=True, null=True)
    proj_code = models.CharField(max_length=100, blank=True, null=True)
    po_number = models.CharField(max_length=200, blank=True, null=True)
    po_date_unf = models.DateTimeField(blank=True, null=True)
    # unf means unformatted
    po_date = models.CharField(max_length=200, blank=True, null=True)
    po_file = models.ManyToManyField(PSFileModel, related_name='po_files')
    design_flag = models.CharField(max_length=30, blank=True, null=True)
    design_flag_entry = models.ManyToManyField('DesignFlag', blank=True)
    production_flag = models.CharField(max_length=30, blank=True, null=True)
    production_flag_reason = models.TextField(blank=True, null=True)
    production_flag_raised_by = models.CharField(max_length=30, blank=True, null=True)
    production_flag_raised_time = models.CharField(max_length=60, blank=True, null=True)
    design_status = models.CharField(max_length=50, blank=True, null=True)
    maker = models.CharField(max_length=50, blank=True, null=True)
    checker = models.CharField(max_length=50, blank=True, null=True)
    op_date_time = models.CharField(max_length=100, blank=True, null=True)
    files = models.ManyToManyField(PSFileModel, related_name='files')
    input_coments = models.TextField(blank=True, null=True)
    delete = models.CharField(max_length=20, default='No', blank=True, null=True)

    def formatted_entry_date(self):
        """Returns the entry date in 'DD-MM-YYYY HH:MM:SS' format"""
        return self.po_date.strftime('%d-%m-%Y %H:%M:%S')


class ds_output(models.Model):
    sr = models.IntegerField()
    sr_id = models.IntegerField(blank=True, null=True)
    rev_no = models.IntegerField(blank=True, null=True)
    maker = models.CharField(max_length=50, blank=True, null=True)
    checker = models.CharField(max_length=50, blank=True, null=True)
    op_date_time = models.CharField(max_length=100, blank=True, null=True)
    files = models.FileField(blank=True, null=True)
    user = models.CharField(max_length=100, blank=True, null=True)
    type = models.CharField(max_length=20, blank=True, null=True)
    strike = models.CharField(max_length=20, blank=True, null=True)


# one entry can have multiple design flag so to store that this table is created
class DesignFlag(models.Model):
    design_flag_reason = models.TextField(blank=True, null=True)
    design_flag_raised_by = models.CharField(max_length=30, blank=True, null=True)
    design_flag_raised_time = models.CharField(max_length=50, blank=True, null=True)
    completed = models.CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return f"Flag by {self.design_flag_raised_by} at {self.design_flag_raised_time}"


class revise_ds_output(models.Model):
    sr = models.IntegerField()
    sr_id = models.IntegerField(blank=True, null=True)
    rev_no = models.IntegerField(blank=True, null=True)
    maker = models.CharField(max_length=50, blank=True, null=True)
    checker = models.CharField(max_length=50, blank=True, null=True)
    op_date_time = models.CharField(max_length=100, blank=True, null=True)
    files = models.FileField(blank=True, null=True)
    user = models.CharField(max_length=100, blank=True, null=True)
    strike = models.CharField(max_length=20, blank=True, null=True)
