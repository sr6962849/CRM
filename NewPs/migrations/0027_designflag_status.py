# Generated by Django 4.2.3 on 2025-02-21 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NewPs', '0026_alter_designflag_design_flag_raised_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='designflag',
            name='status',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
