from django.contrib import admin
from .models import Ps, PSFileModel, ds_output, DesignFlag, revise_ds_output

# Register your models here.
admin.site.register(Ps)
admin.site.register(PSFileModel)
admin.site.register(ds_output)
admin.site.register(revise_ds_output)
admin.site.register(DesignFlag)
