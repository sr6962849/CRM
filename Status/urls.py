from django.urls import path

from . import views

urlpatterns = [
    path('', views.login1, name='login'),
    path('register', views.register, name='register'),
    path('logout', views.logout, name='logout'),
    path('insert', views.insert, name='insert'),
    # it only renders the html
    path('insert1', views.Insertdata, name='insert1'),
    # it inserts the data from webpage to database
    path('showpage', views.showpage, name='showpage'),
    path('showpageUpdate/<int:pk>', views.showpageUpdate, name='showpageUpdate'),
    path('editpage/<int:pk>', views.Editpage, name='editpage'),
    path('update/<int:pk>', views.UpdateData, name='update'),
    path('delete_file/<int:column_id>/<int:file_id>/', views.delete_file, name='delete_file'),
    path('delete/<int:pk>', views.DeleteData, name='delete'),
    path('download/<int:pk>/<int:file_pk>/', views.download_westfile, name='downloadwest'),
    path('view_attachments/<int:pk>/', views.view_attachments, name='view_attachments'),
    path('north_attachments/<int:pk>/', views.north_attachments, name='north_attachments'),
    path('south_attachments/<int:pk>/', views.south_attachments, name='south_attachments'),
    # When we use 'primary key' for selection of row we have to give '<int:pk>' after url

    # name map this name with the hrml page we can add this instead of html page name
    # Till here it was all Column Table which is of West
    path('northShowpage', views.Northshowpage, name='northShowpage'),
    path('northShowpageUpdate/<int:pk>', views.NorthshowpageUpdate, name='northShowpageUpdate'),
    path('northInsert', views.Northinsert, name='northInsert'),
    # it only renders the html
    path('northInsert1', views.NorthInsertdata, name='northInsert1'),
    # it inserts the data from webpage to database
    path('NorthEditpage/<int:pk>', views.NorthEditpage, name='NorthEditpage'),
    path('NorthUpdate/<int:pk>', views.NorthUpdateData, name='NorthUpdate'),
    path('north_delete_file/<int:column_id>/<int:file_id>/', views.north_delete_file, name='north_delete_file'),
    path('NorthDelete/<int:pk>', views.NorthDeleteData, name='NorthDelete'),
    path('downloadnorth/<int:pk>/<int:file_pk>/', views.download_northfile, name='downloadnorth'),
    path('main', views.main1, name='main'),
    # Till here it was all of North Table

    path('southShowpage', views.Southshowpage, name='southShowpage'),
    path('southShowpageUpdate/<int:pk>', views.SouthshowpageUpdate, name='southShowpageUpdate'),
    path('southInsert', views.Southinsert, name='southInsert'),
    path('southInsert1', views.SouthInsertdata, name='southInsert1'),
    path('SouthEditpage/<int:pk>', views.SouthEditpage, name='SouthEditpage'),
    path('SouthUpdate/<int:pk>', views.SouthUpdateData, name='SouthUpdate'),
    path('south_delete_file/<int:column_id>/<int:file_id>/', views.south_delete_file, name='south_delete_file'),
    path('SouthDelete/<int:pk>', views.SouthDeleteData, name='SouthDelete'),
    path('downloadsouth/<int:pk>/<int:file_pk>/', views.download_southfile, name='downloadsouth'),
    path('designer', views.designer, name='designer'),
    path('Northdesign', views.design_north, name='Northdesign'),
    path('Southdesign', views.design_south, name='Southdesign'),
    path('export-to-excel/', views.export_to_excel, name='export_to_excel'),
    path('south-to-excel/', views.south_to_excel, name='south_to_excel'),
    path('north-to-excel/', views.north_to_excel, name='north_to_excel'),
    path('download_all_west_files/<int:pk>/', views.download_all_west_files, name='download_all_west_files'),
    path('download_all_north_files/<int:pk>/', views.download_all_north_files, name='download_all_north_files'),
    path('download_all_south_files/<int:pk>/', views.download_all_south_files, name='download_all_south_files'),
]
