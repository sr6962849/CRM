# myapp/templatetags/custom_filters_n.py
import mimetypes

from django import template

register = template.Library()


@register.filter(name='is_image_or_pdf')
def is_image_or_pdf(file_name):
    mime = mimetypes.guess_type(file_name)[0]
    return mime and (mime.startswith('image') or mime == 'application/pdf')
