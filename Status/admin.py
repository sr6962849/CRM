from django.contrib import admin
from .models import Column, North, South

# Register your models here.
admin.site.register(Column)
admin.site.register(North)
admin.site.register(South)
