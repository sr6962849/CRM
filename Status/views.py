# hey tere
import io
import mimetypes
import os
import zipfile

import pandas as pd
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User, auth
from django.db.models import Max
from django.http import FileResponse, JsonResponse
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from .models import Column, North, South, FileModel


# Create your views here.

def login1(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            request.user = user
            request.session['user_id'] = user.id
            return redirect('main')
        # this is the code to check entered username and password matches with user table database's username and
        # password or not and if it matches then it will redirect it to disp.html
        else:
            messages.error(request, 'Invalid login credentials')
    return render(request, 'login.html')


def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = User.objects.create_user(username=username, password=password)
        user.save()
        return redirect('/')

    else:
        return render(request, 'register.html')


def logout(request):
    auth.logout(request)
    return redirect('/')


def north_user(user):
    return user.groups.filter(name__in=['North', 'Designer']).exists()


def west_user(user):
    return user.groups.filter(name__in=['West', 'Designer']).exists()


def south_user(user):
    return user.groups.filter(name__in=['South', 'Designer']).exists()


# checks whether the user belongs to South or Designer Group


def design_user(user):
    return user.groups.filter(name='Designer').exists()


@user_passes_test(west_user)
def insert(request):
    user = request.user

    # Check if the entered ID already exists in the database
    # all_ids = Column.objects.values_list('id', flat=True)
    all_ids = Column.objects.all()

    return render(request, 'insert.html', {'user': user,
                                           'all_ids': all_ids})


def Insertdata(request):
    result = Column.objects.aggregate(Max('id'))
    latest_id = result['id__max'] if result['id__max'] is not None else 0
    Id = latest_id + 1
    client = request.POST.get('client')
    job_no = request.POST.get('job_no')
    project_name = request.POST.get('project_name')
    capacity = request.POST.get('capacity')
    design_status = request.POST.get('design_status')
    # New Data
    rev = request.POST.get('rev')
    installation_type = request.POST.get('installation_type')
    sales_stage = request.POST.get('sales_stage')
    design_request_mode = request.POST.get('design_request_mode')
    sales_poc = request.POST.get('sales_poc')
    documents_required = request.POST.get('documents_required')
    document_files = request.FILES.getlist('document_file')
    design_assigned = request.POST.get('design_assigned')
    remarks = request.POST.get('remarks')

    newrow = Column.objects.create(id=Id, client=client, job_no=job_no, project_name=project_name,
                                   capacity=capacity, design_status=design_status, rev=rev,
                                   installation_type=installation_type, sales_stage=sales_stage,
                                   design_request_mode=design_request_mode,
                                   sales_poc=sales_poc,
                                   documents_required=documents_required,
                                   design_assigned=design_assigned,
                                   remarks=remarks
                                   )
    # Use set() method to add files to the many-to-many relationship
    for file in document_files:
        file_instance = FileModel.objects.create(document_file=file)
        newrow.document_files.add(file_instance)
    # create method is use, to create new row in database by using this
    # method user can enter the data into database
    return redirect('main')


# insert & InsertData are for the same html page i.e. insert.html
# insert View only renders the insert.html and InsertData View insert the data in database
# and then redirects to show page this thing can be done in one view also, but we have
# done it in two view

@user_passes_test(west_user)
def showpage(request):
    # select * from table name
    all_data = Column.objects.all()
    user = request.user
    westvar = False
    if user.groups.filter(name='West').exists():
        westvar = True

    northvar = False
    if user.groups.filter(name='North').exists():
        northvar = True

    southvar = False
    if user.groups.filter(name='South').exists():
        southvar = True

    intervar = False
    if user.groups.filter(name='International').exists():
        intervar = True

    context = {
        'data': all_data,
        'user': user,
        'westvar': westvar,
        'northvar': northvar,
        'southvar': southvar,
        'intervar': intervar,
    }
    return render(request, 'show.html', context)


def showpageUpdate(request, pk):
    design_update = Column.objects.get(id=pk)
    design_update.design_status = request.POST.get('design_status1')
    design_update.save()
    return redirect('main')


# Edit page view
@user_passes_test(west_user)
def Editpage(request, pk):
    get_data = Column.objects.get(id=pk)
    user = request.user
    # we can fetch the selected data/row with the help of primary key

    all_ids = Column.objects.all()

    salesvar = False
    if user.is_superuser or user.groups.filter(name="West").exists():
        salesvar = True

    context = {
        'get_data': get_data,
        'user': user,
        'salesvar': salesvar,
        'all_ids': all_ids
    }
    return render(request, 'edit.html', context)


def UpdateData(request, pk):
    udata = Column.objects.get(id=pk)
    # we fetch the row that need to be updated with the help of 'primary key'
    udata.id = request.POST.get('id')
    udata.client = request.POST.get('client')
    udata.job_no = request.POST.get('job_no')
    udata.project_name = request.POST.get('project_name')
    udata.capacity = request.POST.get('capacity')
    udata.design_status = request.POST.get('design_status')
    # New data
    udata.rev = request.POST.get('rev')
    udata.installation_type = request.POST.get('installation_type')
    udata.sales_stage = request.POST.get('sales_stage')
    udata.design_request_mode = request.POST.get('design_request_mode')
    udata.sales_poc = request.POST.get('sales_poc')
    udata.documents_required = request.POST.get('documents_required')

    new_document_files = request.FILES.getlist('document_file')
    # Check if a new file is being uploaded
    if new_document_files:
        # Create instances of FileModel for each uploaded file
        for file in new_document_files:
            file_instance = FileModel(document_file=file)
            file_instance.save()
            udata.document_files.add(file_instance)
    # code for editing multiple file by GP

    udata.design_assigned = request.POST.get('design_assigned')
    udata.remarks = request.POST.get('remarks')

    # Query for update
    udata.save()
    # udata has all the updated value and save() method updates the value in database

    # Redirecting to Show Page
    return redirect('main')


# Editpage and UpdateData can be done in the single view, but we have done it in with two view

def delete_file(request, column_id, file_id):
    try:
        column = Column.objects.get(pk=column_id)
        file_instance = column.document_files.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except Column.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


# Delete Data View
def DeleteData(request, pk):
    ddata = Column.objects.get(id=pk)
    # deleting the column
    ddata.delete()
    return redirect('main')


# till here all was of first table which is of West

# Now continuing the North Table
@user_passes_test(north_user)
def Northshowpage(request):
    # select * from table name
    all_data = North.objects.all()
    user = request.user
    westvar = False
    if user.groups.filter(name='West').exists():
        westvar = True

    northvar = False
    if user.groups.filter(name='North').exists():
        northvar = True

    southvar = False
    if user.groups.filter(name='South').exists():
        southvar = True

    intervar = False
    if user.groups.filter(name='International').exists():
        intervar = True

    context = {
        'data': all_data,
        'user': user,
        'westvar': westvar,
        'northvar': northvar,
        'southvar': southvar,
        'intervar': intervar,
    }
    return render(request, 'northShow.html', context)


def NorthshowpageUpdate(request, pk):
    design_update = North.objects.get(id=pk)
    design_update.design_status = request.POST.get('design_status1')
    design_update.save()
    return redirect('Northdesign')


@user_passes_test(north_user)
def Northinsert(request):
    user = request.user
    all_ids = North.objects.all()
    return render(request, 'northInsert.html', {'user': user,
                                                'all_ids': all_ids})


def NorthInsertdata(request):
    result = North.objects.aggregate(Max('id'))
    latest_id = result['id__max'] if result['id__max'] is not None else 0
    Id = latest_id + 1
    client = request.POST.get('client')
    job_no = request.POST.get('job_no')
    project_name = request.POST.get('project_name')
    capacity = request.POST.get('capacity')
    design_status = request.POST.get('design_status')
    # New Data
    rev = request.POST.get('rev')
    installation_type = request.POST.get('installation_type')
    sales_stage = request.POST.get('sales_stage')
    design_request_mode = request.POST.get('design_request_mode')
    sales_poc = request.POST.get('sales_poc')
    documents_required = request.POST.get('documents_required')
    document_files = request.FILES.getlist('document_file')
    design_assigned = request.POST.get('design_assigned')
    remarks = request.POST.get('remarks')

    newrow = North.objects.create(id=Id, client=client, job_no=job_no, project_name=project_name,
                                  capacity=capacity, design_status=design_status, rev=rev,
                                  installation_type=installation_type
                                  , sales_stage=sales_stage, design_request_mode=design_request_mode,
                                  sales_poc=sales_poc,
                                  documents_required=documents_required,
                                  design_assigned=design_assigned,
                                  remarks=remarks)
    # Use set() method to add files to the many-to-many relationship
    for file in document_files:
        file_instance = FileModel.objects.create(document_file=file)
        newrow.document_files.add(file_instance)

    # create method is use, to create new row in database by using this
    # method user can enter the data into database
    return redirect('main')


@user_passes_test(north_user)
def NorthEditpage(request, pk):
    get_data = North.objects.get(id=pk)
    user = request.user

    salesvar = False
    if user.is_superuser or user.groups.filter(name="North").exists():
        salesvar = True

    context = {
        'get_data': get_data,
        'user': user,
        'salesvar': salesvar,
    }
    # we can fetch the selected data/row with the help of primary key
    return render(request, 'northEdit.html', context)


def NorthUpdateData(request, pk):
    udata = North.objects.get(id=pk)
    # we fetch the row that need to be updated with the help of 'primary key'
    udata.id = request.POST.get('id')
    udata.client = request.POST.get('client')
    udata.job_no = request.POST.get('job_no')
    udata.project_name = request.POST.get('project_name')
    udata.capacity = request.POST.get('capacity')
    udata.design_status = request.POST.get('design_status')
    # New data
    udata.rev = request.POST.get('rev')
    udata.installation_type = request.POST.get('installation_type')
    udata.sales_stage = request.POST.get('sales_stage')
    udata.design_request_mode = request.POST.get('design_request_mode')
    udata.sales_poc = request.POST.get('sales_poc')
    udata.documents_required = request.POST.get('documents_required')

    new_document_files = request.FILES.getlist('document_file')
    # Check if a new file is being uploaded
    if new_document_files:
        # Create instances of FileModel for each uploaded file
        for file in new_document_files:
            file_instance = FileModel(document_file=file)
            file_instance.save()
            udata.document_files.add(file_instance)
    # code for editing multiple file by GP

    udata.design_assigned = request.POST.get('design_assigned')
    udata.remarks = request.POST.get('remarks')
    # Query for update
    udata.save()
    # udata has all the updated value and save() method updates the value in database

    # Redirecting to Show Page
    return redirect('main')


def north_delete_file(request, column_id, file_id):
    try:
        column = North.objects.get(pk=column_id)
        file_instance = column.document_files.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except North.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


def NorthDeleteData(request, pk):
    ddata = North.objects.get(id=pk)
    # deleting the column
    ddata.delete()
    return redirect('Northdesign')


def main1(request):
    user = request.user
    if request.user.is_authenticated:
        if user.groups.filter(name='North').exists():
            return redirect('northShowpage')
        elif user.groups.filter(name='West').exists():
            return redirect('showpage')
        elif user.groups.filter(name='South').exists():
            return redirect('southShowpage')
        elif user.groups.filter(name='Designer').exists():
            return redirect('designer')
        elif user.groups.filter(name='International').exists():
            return redirect('internationalShow')
        elif user.groups.filter(name='Production').exists():
            return redirect('ps1')
        elif user.groups.filter(name__in=['NWest', 'NNorth', 'NSouth', 'NInternational']).exists():
            return redirect('newDesign')
        elif user.groups.filter(name='NDesign').exists():
            return redirect('newdesShow')
        elif user.groups.filter(name='NProduction').exists():
            return redirect('newPs')

        return render(request, 'Main.html')
    else:
        # The user is not logged in, show an appropriate message or redirect to the login page.
        return HttpResponse('Please log in to view the tables.')


# till here all was of Second table which is of North
# from here of South Table
@user_passes_test(south_user, design_user)
def Southshowpage(request):
    # select * from table name
    all_data = South.objects.all()
    user = request.user
    westvar = False
    if user.groups.filter(name='West').exists():
        westvar = True

    northvar = False
    if user.groups.filter(name='North').exists():
        northvar = True

    southvar = False
    if user.groups.filter(name='South').exists():
        southvar = True

    intervar = False
    if user.groups.filter(name='International').exists():
        intervar = True

    context = {
        'data': all_data,
        'user': user,
        'westvar': westvar,
        'northvar': northvar,
        'southvar': southvar,
        'intervar': intervar,
    }
    return render(request, 'southShow.html', context)


def SouthshowpageUpdate(request, pk):
    design_update = South.objects.get(id=pk)
    design_update.design_status = request.POST.get('design_status1')
    design_update.save()
    return redirect('Southdesign')


@user_passes_test(south_user)
def Southinsert(request):
    user = request.user
    all_ids = South.objects.all()
    return render(request, 'southInsert.html', {'user': user,
                                                'all_ids': all_ids})


def SouthInsertdata(request):
    result = South.objects.aggregate(Max('id'))
    latest_id = result['id__max'] if result['id__max'] is not None else 0
    Id = latest_id + 1
    client = request.POST.get('client')
    job_no = request.POST.get('job_no')
    project_name = request.POST.get('project_name')
    capacity = request.POST.get('capacity')
    design_status = request.POST.get('design_status')
    # New Data
    rev = request.POST.get('rev')
    installation_type = request.POST.get('installation_type')
    sales_stage = request.POST.get('sales_stage')
    design_request_mode = request.POST.get('design_request_mode')
    sales_poc = request.POST.get('sales_poc')
    documents_required = request.POST.get('documents_required')
    document_files = request.FILES.getlist('document_file')
    design_assigned = request.POST.get('design_assigned')
    remarks = request.POST.get('remarks')

    newrow = South.objects.create(id=Id, client=client, job_no=job_no, project_name=project_name,
                                  capacity=capacity, design_status=design_status, rev=rev,
                                  installation_type=installation_type
                                  , sales_stage=sales_stage, design_request_mode=design_request_mode,
                                  sales_poc=sales_poc,
                                  documents_required=documents_required,
                                  design_assigned=design_assigned,
                                  remarks=remarks)
    # Use set() method to add files to the many-to-many relationship
    for file in document_files:
        file_instance = FileModel.objects.create(document_file=file)
        newrow.document_files.add(file_instance)

    # create method is use, to create new row in database by using this
    # method user can enter the data into database
    return redirect('main')


@user_passes_test(south_user)
def SouthEditpage(request, pk):
    get_data = South.objects.get(id=pk)
    user = request.user
    # we can fetch the selected data/row with the help of primary key
    salesvar = False
    if user.is_superuser or user.groups.filter(name="South").exists():
        salesvar = True

    context = {
        'get_data': get_data,
        'user': user,
        'salesvar': salesvar,
    }
    return render(request, 'southEdit.html', context)


def SouthUpdateData(request, pk):
    udata = South.objects.get(id=pk)
    # we fetch the row that need to be updated with the help of 'primary key'
    udata.id = request.POST.get('id')
    udata.client = request.POST.get('client')
    udata.job_no = request.POST.get('job_no')
    udata.project_name = request.POST.get('project_name')
    udata.capacity = request.POST.get('capacity')
    udata.design_status = request.POST.get('design_status')
    # New data
    udata.rev = request.POST.get('rev')
    udata.installation_type = request.POST.get('installation_type')
    udata.sales_stage = request.POST.get('sales_stage')
    udata.design_request_mode = request.POST.get('design_request_mode')
    udata.sales_poc = request.POST.get('sales_poc')
    udata.documents_required = request.POST.get('documents_required')

    new_document_files = request.FILES.getlist('document_file')
    # Check if a new file is being uploaded
    if new_document_files:
        # Create instances of FileModel for each uploaded file
        for file in new_document_files:
            file_instance = FileModel(document_file=file)
            file_instance.save()
            udata.document_files.add(file_instance)
    # code for editing multiple file by GP

    udata.design_assigned = request.POST.get('design_assigned')
    udata.remarks = request.POST.get('remarks')
    # Query for update
    udata.save()
    # udata has all the updated value and save() method updates the value in database

    # Redirecting to Show Page
    return redirect('main')


def south_delete_file(request, column_id, file_id):
    try:
        column = South.objects.get(pk=column_id)
        file_instance = column.document_files.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except South.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


def SouthDeleteData(request, pk):
    ddata = South.objects.get(id=pk)
    # deleting the column
    ddata.delete()
    return redirect('Southdesign')


@user_passes_test(design_user)
def designer(request):
    user = request.user
    all_data = Column.objects.all()
    excluded_data = Column.objects.exclude(design_status='Done')
    # it will fetch all row but exclude rows in which design_status is 'Done'
    return render(request, 'designer.html', {'user': user,
                                             'all_data': all_data, 'excluded_data': excluded_data})


@user_passes_test(design_user)
def design_north(request):
    user = request.user
    north_data = North.objects.all()
    excluded_data = North.objects.exclude(design_status='Done')
    return render(request, 'design_north.html', {'user': user,
                                                 'north_data': north_data, 'excluded_data': excluded_data})


@user_passes_test(design_user)
def design_south(request):
    user = request.user
    south_data = South.objects.all()
    excluded_data = South.objects.exclude(design_status='Done')
    return render(request, 'design_south.html', {'user': user,
                                                 'south_data': south_data, 'excluded_data': excluded_data})


# used to download table as a excel
def export_to_excel(request):
    # Assuming you have a function to fetch your table data as a queryset
    queryset = Column.objects.all()

    # Convert the queryset to a Pandas DataFrame
    df = pd.DataFrame.from_records(queryset.values())

    # Create a response with an Excel attachment
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="west_table_data.xlsx"'

    # Write the DataFrame to the response
    df.to_excel(response, index=False, engine='openpyxl')

    return response


def south_to_excel(request):
    # Assuming you have a function to fetch your table data as a queryset
    queryset = South.objects.all()

    # Convert the queryset to a Pandas DataFrame
    df = pd.DataFrame.from_records(queryset.values())

    # Create a response with an Excel attachment
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="south_table_data.xlsx"'

    # Write the DataFrame to the response
    df.to_excel(response, index=False, engine='openpyxl')

    return response


def north_to_excel(request):
    # Assuming you have a function to fetch your table data as a queryset
    queryset = North.objects.all()

    # Convert the queryset to a Pandas DataFrame
    df = pd.DataFrame.from_records(queryset.values())

    # Create a response with an Excel attachment
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="north_table_data.xlsx"'

    # Write the DataFrame to the response
    df.to_excel(response, index=False, engine='openpyxl')

    return response


def download_westfile(request, pk, file_pk):
    instance = get_object_or_404(Column, id=pk)
    file_instance = get_object_or_404(FileModel, id=file_pk)

    response = FileResponse(file_instance.document_file)
    mime_type, _ = mimetypes.guess_type(file_instance.document_file.name)
    response['Content-Type'] = mime_type

    # Only set Content-Disposition for non-image/non-PDF files
    if not (mime_type and (mime_type.startswith('image') or mime_type == 'application/pdf')):
        response['Content-Disposition'] = f'attachment; filename="{file_instance.document_file.name}"'

    return response


def download_northfile(request, pk, file_pk):
    instance = get_object_or_404(Column, id=pk)
    file_instance = get_object_or_404(FileModel, id=file_pk)

    response = FileResponse(file_instance.document_file)
    mime_type, _ = mimetypes.guess_type(file_instance.document_file.name)
    response['Content-Type'] = mime_type

    if not (mime_type and (mime_type.startswith('image') or mime_type == 'application/pdf')):
        response['Content-Disposition'] = f'attachment; filename="{file_instance.document_file.name}"'

    return response


def download_southfile(request, pk, file_pk):
    instance = get_object_or_404(Column, id=pk)
    file_instance = get_object_or_404(FileModel, id=file_pk)

    response = FileResponse(file_instance.document_file)
    mime_type, _ = mimetypes.guess_type(file_instance.document_file.name)
    response['Content-Type'] = mime_type

    if not (mime_type and (mime_type.startswith('image') or mime_type == 'application/pdf')):
        response['Content-Disposition'] = f'attachment; filename="{file_instance.document_file.name}"'

    return response


def view_attachments(request, pk):
    all_data = Column.objects.filter(id=pk)
    return render(request, 'Attachment.html', {'data': all_data})


def north_attachments(request, pk):
    all_data = North.objects.filter(id=pk)
    return render(request, 'Attachment.html', {'north_data': all_data})


def south_attachments(request, pk):
    all_data = South.objects.filter(id=pk)
    return render(request, 'Attachment.html', {'south_data': all_data})


def download_all_west_files(request, pk):
    instance = get_object_or_404(Column, id=pk)
    files = instance.document_files.all()

    # Create a zip file in memory
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zip_file:
        for file_instance in files:
            file_path = file_instance.document_file.path
            file_name = os.path.basename(file_path)
            zip_file.write(file_path, arcname=file_name)

    # Construct response
    zip_buffer.seek(0)
    response = HttpResponse(zip_buffer, content_type='application/zip')
    response['Content-Disposition'] = f'attachment; filename="files.zip"'
    return response


def download_all_north_files(request, pk):
    instance = get_object_or_404(North, id=pk)
    files = instance.document_files.all()

    # Create a zip file in memory
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zip_file:
        for file_instance in files:
            file_path = file_instance.document_file.path
            file_name = os.path.basename(file_path)
            zip_file.write(file_path, arcname=file_name)

    # Construct response
    zip_buffer.seek(0)
    response = HttpResponse(zip_buffer, content_type='application/zip')
    response['Content-Disposition'] = f'attachment; filename="files.zip"'
    return response


def download_all_south_files(request, pk):
    instance = get_object_or_404(South, id=pk)
    files = instance.document_files.all()

    # Create a zip file in memory
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zip_file:
        for file_instance in files:
            file_path = file_instance.document_file.path
            file_name = os.path.basename(file_path)
            zip_file.write(file_path, arcname=file_name)

    # Construct response
    zip_buffer.seek(0)
    response = HttpResponse(zip_buffer, content_type='application/zip')
    response['Content-Disposition'] = f'attachment; filename="files.zip"'
    return response
