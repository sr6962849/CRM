# Generated by Django 4.2.3 on 2023-09-14 11:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Status', '0005_column_design_request_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='north',
            name='design_assigned',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='design_request_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='design_request_mode',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='documents_required',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='installation_type',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='remarks',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='rev',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='sales_poc',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='north',
            name='sales_stage',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='design_assigned',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='design_request_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='design_request_mode',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='documents_required',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='installation_type',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='remarks',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='rev',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='sales_poc',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='south',
            name='sales_stage',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
