# Generated by Django 4.2.3 on 2024-01-04 08:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Status', '0014_rename_document_files_column_document_file_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filemodel',
            name='document_file',
            field=models.FileField(upload_to=''),
        ),
    ]
