from django.db import models


# Create your models here.
class FileModel(models.Model):
    document_file = models.FileField(upload_to='')


class International(models.Model):
    sr = models.IntegerField()
    client = models.CharField(max_length=200, blank=False, null=False)
    job_no = models.CharField(max_length=100, blank=True, null=True)
    project_name = models.CharField(max_length=100, blank=True, null=True)
    capacity = models.CharField(max_length=100, blank=True, null=True)
    design_status = models.CharField(max_length=50, blank=False, null=False)
    # New
    rev = models.CharField(max_length=50, blank=True, null=True)
    installation_type = models.CharField(max_length=50, blank=True, null=True)
    sales_stage = models.CharField(max_length=50, blank=True, null=True)
    design_request_mode = models.CharField(max_length=50, blank=True, null=True)
    sales_poc = models.CharField(max_length=50, blank=True, null=True)
    documents_required = models.CharField(max_length=100, blank=True, null=True)
    document_files = models.ManyToManyField(FileModel)
    design_assigned = models.CharField(max_length=100, blank=True, null=True)
    remarks = models.CharField(max_length=2000, blank=True, null=True)
    design_request_date = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return f"{self.client} - {self.project_name}"
