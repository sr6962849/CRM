from django.urls import path
from . import views

urlpatterns = [
    path('internationalShow', views.InternationalShow, name='internationalShow'),
    path('intershowpageUpdate/<int:pk>', views.intershowpageUpdate, name='intershowpageUpdate'),
    path('interInsert', views.interInsert, name='interInsert'),
    # it only renders the html
    path('interInsert1', views.interInsertdata, name='interInsert1'),
    # it inserts the data from webpage to database
    path('interedit/<int:pk>', views.interEditpage, name='interedit'),
    path('interupdate/<int:pk>', views.interUpdateData, name='interupdate'),
    path('inter_delete_file/<int:column_id>/<int:file_id>/', views.inter_delete_file, name='inter_delete_file'),
    path('interdelete/<int:pk>', views.interDeleteData, name='interdelete'),
    path('inter_attachments/<int:pk>/', views.inter_attachments, name='inter_attachments'),
    path('interdownload/<int:pk>/<int:file_pk>/', views.download_interfile, name='interdownload'),
    path('download_all_files/<int:pk>/', views.download_all_files, name='download_all_files'),
    path('interDesigner', views.interDesigner, name='interDesigner'),
    path('inter_to_excel/', views.inter_to_excel, name='inter_to_excel')

]
