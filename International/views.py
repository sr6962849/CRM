import io
import mimetypes
import os
import zipfile

import pandas as pd
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.db.models import Max
from django.http import FileResponse, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from Status.views import design_user
from .models import International, FileModel


# Create your views here.
def international_user(user):
    return user.groups.filter(name__in=['International', 'Designer']).exists()


@user_passes_test(international_user)
def InternationalShow(request):
    # select * from table name
    all_data = International.objects.all()
    user = request.user
    westvar = False
    if user.groups.filter(name='West').exists():
        westvar = True

    northvar = False
    if user.groups.filter(name='North').exists():
        northvar = True

    southvar = False
    if user.groups.filter(name='South').exists():
        southvar = True

    intervar = False
    if user.groups.filter(name='International').exists():
        intervar = True

    context = {
        'data': all_data,
        'user': user,
        'westvar': westvar,
        'northvar': northvar,
        'southvar': southvar,
        'intervar': intervar,
    }
    return render(request, 'InternationShow.html', context)


def intershowpageUpdate(request, pk):
    design_update = International.objects.get(sr=pk)
    design_update.design_status = request.POST.get('design_status1')
    design_update.save()
    return redirect('interDesigner')


@user_passes_test(international_user)
def interInsert(request):
    return render(request, 'InterInsert.html')


@user_passes_test(international_user)
def interInsertdata(request):
    result = International.objects.aggregate(Max('sr'))
    latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
    # maximum id/sr is retrieved from database than it is assigned to latest_sr
    # if it is noth there than 0 is assigned and then later we add 1
    # each time we create row
    sr = latest_sr + 1
    client = request.POST.get('client')
    job_no = request.POST.get('job_no')
    project_name = request.POST.get('project_name')
    capacity = request.POST.get('capacity')
    design_status = request.POST.get('design_status')
    # New Data
    rev = request.POST.get('rev')
    installation_type = request.POST.get('installation_type')
    sales_stage = request.POST.get('sales_stage')
    design_request_mode = request.POST.get('design_request_mode')
    sales_poc = request.POST.get('sales_poc')
    documents_required = request.POST.get('documents_required')
    document_files = request.FILES.getlist('document_file')
    design_assigned = request.POST.get('design_assigned')
    remarks = request.POST.get('remarks')
    design_request_date = request.POST.get('design_request_date')

    newrow = International.objects.create(sr=sr, client=client, job_no=job_no, project_name=project_name,
                                          capacity=capacity, design_status=design_status, rev=rev,
                                          installation_type=installation_type, sales_stage=sales_stage,
                                          design_request_mode=design_request_mode,
                                          sales_poc=sales_poc,
                                          documents_required=documents_required,
                                          design_assigned=design_assigned,
                                          remarks=remarks
                                          , design_request_date=design_request_date)
    # Use set() method to add files to the many-to-many relationship
    for file in document_files:
        file_instance = FileModel.objects.create(document_file=file)
        newrow.document_files.add(file_instance)
    # create method is use, to create new row in database by using this
    # method user can enter the data into database
    return redirect('main')


@user_passes_test(international_user)
def interEditpage(request, pk):
    get_data = International.objects.get(sr=pk)
    # we can fetch the selected data/row with the help of primary key
    user_id = request.session.get('user_id')
    # user id is retrieved from session
    user1 = User.objects.get(id=user_id)
    # and from user id, usernamer is retrieved and will be printed there
    context = {
        'get_data': get_data,
        'user': user1,
    }
    return render(request, 'interEdit.html', context)


def interUpdateData(request, pk):
    udata = International.objects.get(sr=pk)
    # we fetch the row that need to be updated with the help of 'primary key'
    udata.client = request.POST.get('client')
    udata.job_no = request.POST.get('job_no')
    udata.project_name = request.POST.get('project_name')
    udata.capacity = request.POST.get('capacity')
    udata.design_status = request.POST.get('design_status')
    # New data
    udata.rev = request.POST.get('rev')
    udata.installation_type = request.POST.get('installation_type')
    udata.sales_stage = request.POST.get('sales_stage')
    udata.design_request_mode = request.POST.get('design_request_mode')
    udata.sales_poc = request.POST.get('sales_poc')
    udata.documents_required = request.POST.get('documents_required')

    new_document_files = request.FILES.getlist('document_file')
    # Check if a new file is being uploaded
    if new_document_files:
        # Create instances of FileModel for each uploaded file
        for file in new_document_files:
            file_instance = FileModel(document_file=file)
            file_instance.save()
            udata.document_files.add(file_instance)
    # code for editing multiple file by GP

    udata.design_assigned = request.POST.get('design_assigned')
    udata.remarks = request.POST.get('remarks')
    udata.design_request_date = request.POST.get('design_request_date')
    # Query for update
    udata.save()
    # udata has all the updated value and save() method updates the value in database

    # Redirecting to Show Page
    return redirect('main')


def inter_delete_file(request, column_id, file_id):
    try:
        column = International.objects.get(sr=column_id)
        file_instance = column.document_files.get(pk=file_id)
        # Delete the associated file from the storage
        file_path = os.path.join(settings.MEDIA_ROOT, str(file_instance.document_file))
        if os.path.exists(file_path):
            os.remove(file_path)
        file_instance.delete()
        return redirect(request.META.get('HTTP_REFERER', '/'))
    except International.DoesNotExist:
        return JsonResponse({'error': 'Column not found'}, status=404)
    except FileModel.DoesNotExist:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)


def interDeleteData(request, pk):
    ddata = International.objects.get(sr=pk)
    # deleting the column
    ddata.delete()
    return redirect('interDesigner')


def inter_attachments(request, pk):
    inter_data = International.objects.filter(sr=pk)
    context = {
        'inter_data': inter_data,
    }
    return render(request, 'Attachment.html', context)


def download_interfile(request, pk, file_pk):
    instance = get_object_or_404(International, sr=pk)
    file_instance = get_object_or_404(FileModel, id=file_pk)

    response = FileResponse(file_instance.document_file)
    mime_type, _ = mimetypes.guess_type(file_instance.document_file.name)
    response['Content-Type'] = mime_type

    if not (mime_type and (mime_type.startswith('image') or mime_type == 'application/pdf')):
        response['Content-Disposition'] = f'attachment; filename="{file_instance.document_file.name}"'

    return response


@user_passes_test(design_user)
def interDesigner(request):
    all_data = International.objects.all()
    excluded_data = International.objects.exclude(design_status='Done')
    # it will fetch all row but exclude rows in which design_status is 'Done'

    context = {
        'all_data': all_data,
        'excluded_data': excluded_data,
    }
    return render(request, 'design_inter.html', context)


def inter_to_excel(request):
    # Assuming you have a function to fetch your table data as a queryset
    queryset = International.objects.all()

    # Convert the queryset to a Pandas DataFrame
    df = pd.DataFrame.from_records(queryset.values())

    # Create a response with an Excel attachment
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="International_table_data.xlsx"'

    # Write the DataFrame to the response
    df.to_excel(response, index=False, engine='openpyxl')

    return response


def download_all_files(request, pk):
    instance = get_object_or_404(International, sr=pk)
    files = instance.document_files.all()

    # Create a zip file in memory
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zip_file:
        for file_instance in files:
            file_path = file_instance.document_file.path
            file_name = os.path.basename(file_path)
            zip_file.write(file_path, arcname=file_name)

    # Construct response
    zip_buffer.seek(0)
    response = HttpResponse(zip_buffer, content_type='application/zip')

    # Use a different attribute for the zip file name, e.g., 'sr' or any other relevant attribute
    response['Content-Disposition'] = f'attachment; filename="{instance.sr}_files.zip"'
    return response
