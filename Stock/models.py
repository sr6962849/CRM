from django.db import models


# Create your models here.
class Data(models.Model):
    ma_code = models.CharField(max_length=200, blank=True, null=True)
    item_name = models.CharField(max_length=2000, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    wt_rm = models.CharField(max_length=200, blank=True, null=True)
    length = models.CharField(max_length=200, blank=True, null=True)
    Qty = models.CharField(max_length=200, blank=True, null=True)
    tot_Weight = models.CharField(max_length=200, blank=True, null=True)

    def save(self, *args, **kwargs):
        # Check if all required fields are not None
        if self.wt_rm is not None and self.length is not None and self.Qty is not None:
            try:
                # Calculate total weight
                total_weight = float(self.wt_rm) * float(self.length) * float(self.Qty)
                # Round off to 2 decimal places and convert to string
                self.tot_Weight = str(round(total_weight, 2))
            except ValueError:
                # Handle if any of the fields cannot be converted to float
                self.tot_Weight = None
        else:
            self.tot_Weight = None

        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.ma_code} - {self.item_name} | {self.wt_rm} | {self.length} | {self.Qty} | {self.tot_Weight}"


class Entry(models.Model):
    sr = models.IntegerField(blank=True, null=True)
    client_name = models.CharField(max_length=1000, blank=True, null=True)
    po_ref = models.CharField(max_length=1000, blank=True, null=True)
    dc_no = models.CharField(max_length=1000, blank=True, null=True)
    delivery_add = models.TextField(blank=True, null=True)
    date = models.DateField(blank=True, null=True, auto_now_add=True)
    boxes = models.ManyToManyField('Box', related_name='entries')
    tot_weight = models.CharField(max_length=1000, blank=True, null=True)

    def __str__(self):
        return f"{self.dc_no} - {self.client_name} - {self.date}"


class Box(models.Model):
    box_type = models.CharField(max_length=80, blank=True, null=True)
    box_no = models.CharField(max_length=1000, blank=True, null=True)
    radio_type = models.CharField(max_length=200, blank=True, null=True)
    ma_code = models.CharField(max_length=1000, blank=True, null=True)
    fastner = models.CharField(max_length=1000, blank=True, null=True)
    length = models.CharField(max_length=1000, blank=True, null=True)
    qty = models.CharField(max_length=1000, blank=True, null=True)
    weight = models.CharField(max_length=1000, blank=True, null=True)
    tot_box_weight = models.CharField(max_length=1000, blank=True, null=True)
    items = models.ManyToManyField('Item', related_name='boxes')


class Item(models.Model):
    radio_type = models.CharField(max_length=200, blank=True, null=True)
    ma_code = models.CharField(max_length=1000, blank=True, null=True)
    fastner = models.CharField(max_length=1000, blank=True, null=True)
    length = models.CharField(max_length=1000, blank=True, null=True)
    qty = models.CharField(max_length=1000, blank=True, null=True)
    weight = models.CharField(max_length=1000, blank=True, null=True)


class Fastners(models.Model):
    material = models.CharField(max_length=300, blank=True, null=True)
    identifier_1 = models.CharField(max_length=300, blank=True, null=True)
    identifier_2 = models.CharField(max_length=300, blank=True, null=True)
    identifier_3 = models.CharField(max_length=300, blank=True, null=True)
    identifier_4 = models.CharField(max_length=300, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    weight = models.CharField(max_length=300, blank=True, null=True)
    rate_pc = models.CharField(max_length=300, blank=True, null=True)
    Remark = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.name} - {self.material}"
