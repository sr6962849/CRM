import base64
import json
from io import BytesIO
from json import JSONDecodeError

import qrcode
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import Max
from django.shortcuts import render, redirect

from .models import Data, Entry, Box, Fastners, Item


# Create your views here.

def stockLogin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            request.user = user
            request.session['user_id'] = user.id
            return redirect('stockins')
        # this is the code to check entered username and password matches with user table database's username and
        # password or not and if it matches then it will redirect it to disp.html
        else:
            messages.error(request, 'Invalid login credentials')
    return render(request, 'stockLogin.html')


@login_required
def stockins(request):
    # user = request.user
    # Assuming your model is named "Column" and it has a column named "ma_code"
    ma_code_values_unf = Data.objects.values('ma_code', 'item_name').distinct()
    ma_code_values = [f"{item['ma_code']} - {item['item_name']}" for item in ma_code_values_unf]
    allen_head_bolts = Fastners.objects.filter(identifier_4="Allen Head Bolts").values_list('name', flat=True)
    hex_head_bolts = Fastners.objects.filter(identifier_4="Hex Head Bolts").values_list('name', flat=True)
    hex_nuts = Fastners.objects.filter(identifier_4="Hex Nuts").values_list('name', flat=True)
    washers = Fastners.objects.filter(identifier_4="Washers").values_list('name', flat=True)
    sds = Fastners.objects.filter(identifier_4="SDS").values_list('name', flat=True)
    grub_screw = Fastners.objects.filter(identifier_4="Grub Screw").values_list('name', flat=True)
    rivet = Fastners.objects.filter(identifier_4="Rivet").values_list('name', flat=True)
    epdm = Fastners.objects.filter(identifier_4="EPDM").values_list('name', flat=True)
    grounding_clip = Fastners.objects.filter(identifier_4="Grounding Clip").values_list('name', flat=True)
    chemical = Fastners.objects.filter(identifier_4="Chemical").values_list('name', flat=True)
    context = {
        'ma_code_values': ma_code_values,
        'allen_head_bolts': allen_head_bolts,
        'hex_head_bolts': hex_head_bolts,
        'hex_nuts': hex_nuts,
        'washers': washers,
        'sds': sds,
        'grub_screw': grub_screw,
        'rivet': rivet,
        'epdm': epdm,
        'grounding_clip': grounding_clip,
        'chemical': chemical,
    }

    return render(request, 'StockInsert.html', context)


def stock_entry(request):
    if request.method == 'POST':
        # Get data from the form
        client_name = request.POST.get('client_name')
        po_ref = request.POST.get('po_ref')
        dc_no = request.POST.get('dc_no')
        delivery_add = request.POST.get('delivery_add')
        box_inc = int(request.POST.get('hiddenValue'))
        item_value_str = request.POST.get('itemValue', '')
        # Retrieve boxItemCounts as JSON string
        box_item_counts_json = request.POST.get('boxItemCounts', '[]')  # Provide a default value
        try:
            box_item_counts = json.loads(box_item_counts_json)
        except JSONDecodeError:
            box_item_counts = []  # Handle the error by setting a default value
        if item_value_str:
            try:
                item_inc = int(item_value_str)
            except ValueError:
                # Handle the case where conversion fails, if necessary
                item_inc = 0  # or some default value
        else:
            item_inc = 0  # or some default value
        # if user didn't add and item than item value was coming empty
        # so error was coming so we change it that if it is empty it should take 0

        result = Entry.objects.aggregate(Max('sr'))
        latest_sr = result['sr__max'] if result['sr__max'] is not None else 0
        sr = latest_sr + 1

        # Create an Entry instance
        entry = Entry.objects.create(sr=sr, client_name=client_name, po_ref=po_ref,
                                     dc_no=dc_no, delivery_add=delivery_add)

        # Get box details
        box_type = [request.POST.get(f'box_type{i}') for i in range(1, box_inc + 1)]
        box_no = [request.POST.get(f'box_no{i}') for i in range(1, box_inc + 1)]
        box_radio_type = [request.POST.get(f'ma-code{i}') for i in range(1, box_inc + 1)]
        box_ma_code = [request.POST.get(f'ma-code_opt{i}') for i in range(1, box_inc + 1)]
        box_ma_code_filt = [code.split(' - ')[0] for code in box_ma_code]
        box_fastner = [request.POST.get(f'fastner_opt{i}') for i in range(1, box_inc + 1)]
        box_fastner_sort = [request.POST.get(f'fastner_sort{i}') for i in range(1, box_inc + 1)]
        box_length_ma = [
            float(request.POST.get(f'length{i}', 0) or 0) / 1000
            for i in range(1, box_inc + 1)
        ]
        box_qty = [request.POST.get(f'qty{i}') for i in range(1, box_inc + 1)]

        # Create a list of wt_rm values corresponding to the ma_codes
        box_wt_rm = []
        for code in box_ma_code_filt:
            if code:  # Only process non-empty ma_code
                try:
                    data_obj = Data.objects.filter(ma_code=code).first()
                    if data_obj:
                        box_wt_rm.append(data_obj.wt_rm)
                    else:
                        box_wt_rm.append(None)  # Handle case where ma_code is not found
                except Data.DoesNotExist:
                    box_wt_rm.append(0)  # Handle case where ma_code is not found
            else:
                box_wt_rm.append(None)  # Handle empty ma_code

        box_weight_ma = []
        for x, y, z in zip(box_length_ma, box_qty, box_wt_rm):
            try:
                x_value = float(x) if x is not None and x != '' else 0
                y_value = float(y) if y is not None and y != '' else 0
                z_value = float(z) if z is not None and z != '' else 0
                box_weight_ma.append(round(x_value * y_value * z_value, 2))
            except (ValueError, TypeError):
                box_weight_ma.append(None)

        box_fastner_weight = []
        box_fastner_length = []
        for identifier, name in zip(box_fastner, box_fastner_sort):
            if identifier and name:  # Only process non-empty identifiers and names
                try:
                    fastner_obj = Fastners.objects.get(identifier_4=identifier, name=name)

                    box_fastner_weight.append(float(fastner_obj.weight))
                    box_fastner_length.append(fastner_obj.identifier_2)
                except Fastners.DoesNotExist:
                    box_fastner_weight.append(None)  # Handle case where fastner is not found
                    box_fastner_length.append(None)
            else:
                box_fastner_weight.append(None)  # Handle empty identifier or name
                box_fastner_length.append(None)

        box_weight = []

        box_length = []

        # Iterate through the items with index
        for idx, radio_type in enumerate(box_radio_type):
            if radio_type == 'ma-code':
                box_weight.append(box_weight_ma[idx])
            elif radio_type == 'fastner':
                box_weight.append(round(box_fastner_weight[idx] * float(box_qty[idx]), 2))
            else:
                box_weight.append(None)  # Handle case where radio type is neither "ma-code" nor "fastner"

        # Iterate through the items with index
        for idx, radio_type in enumerate(box_radio_type):
            if radio_type == 'ma-code':
                box_length.append(box_length_ma[idx])
            elif radio_type == 'fastner':
                box_length.append(box_fastner_length[idx])
            else:
                box_length.append(None)
        # here we run two loops because we have two data one of ma-code and one of fastner
        # so it will add data on the basis of which radio button is selected

        box_fastner_new = [f'{x} -> {y}' for x, y in zip(box_fastner, box_fastner_sort)]

        tot_weight = sum(box_weight)
        entry.tot_weight = tot_weight
        entry.save()

        # Get item details
        item_radio_type = [request.POST.get(f'ma-codeitem{i}') for i in range(1, item_inc + 1)]
        item_ma_code = [request.POST.get(f'ma-code_optitem{i}') for i in range(1, item_inc + 1)]
        item_fastner = [request.POST.get(f'fastner_optitem{i}') for i in range(1, item_inc + 1)]
        item_fastner_sort = [request.POST.get(f'fastner_sortitem{i}') for i in range(1, item_inc + 1)]
        item_length_ma = [
            float(request.POST.get(f'lengthitem{i}', 0) or 0) / 1000
            for i in range(1, item_inc + 1)
        ]
        item_qty = [request.POST.get(f'qtyitem{i}') for i in range(1, item_inc + 1)]
        # Create a list of wt_rm values corresponding to the ma_codes
        item_wt_rm = []
        for code in item_ma_code:
            if code:  # Only process non-empty ma_code
                try:
                    data_obj = Data.objects.filter(ma_code=code).first()
                    if data_obj:
                        item_wt_rm.append(data_obj.wt_rm)
                    else:
                        item_wt_rm.append(None)  # Handle case where ma_code is not found
                except Data.DoesNotExist:
                    item_wt_rm.append(0)  # Handle case where ma_code is not found
            else:
                item_wt_rm.append(None)  # Handle empty ma_code

        item_weight_ma = []

        for x, y, z in zip(item_length_ma, item_qty, item_wt_rm):
            try:
                x_value = float(x) if x is not None and x != '' else 0
                y_value = float(y) if y is not None and y != '' else 0
                z_value = float(z) if z is not None and z != '' else 0
                item_weight_ma.append(round(x_value * y_value * z_value, 2))
            except ValueError:
                item_weight_ma.append(None)
        item_fastner_weight = []
        item_fastner_length = []
        for identifier, name in zip(item_fastner, item_fastner_sort):
            if identifier and name:  # Only process non-empty identifiers and names
                try:
                    fastner_obj = Fastners.objects.get(identifier_4=identifier, name=name)
                    item_fastner_weight.append(float(fastner_obj.weight))
                    item_fastner_length.append(fastner_obj.identifier_2)
                except Fastners.DoesNotExist:
                    item_fastner_weight.append(None)  # Handle case where fastner is not found
                    item_fastner_length.append(None)
            else:
                item_fastner_weight.append(None)  # Handle empty identifier or name
                item_fastner_length.append(None)

        item_weight = []
        item_length = []

        for idx, radio_type in enumerate(item_radio_type):
            if radio_type == 'ma-code':
                item_weight.append(item_weight_ma[idx])
            elif radio_type == 'fastner':
                item_weight.append(round(item_fastner_weight[idx] * float(item_qty[idx]), 2))
            else:
                box_weight.append(None)  # Handle case where radio type is neither "ma-code" nor "fastner"

        # Iterate through the items with index
        for idx, radio_type in enumerate(item_radio_type):
            if radio_type == 'ma-code':
                item_length.append(item_length_ma[idx])
            elif radio_type == 'fastner':
                item_length.append(item_fastner_length[idx])
            else:
                item_length.append(None)

        item_fastner_new = [f'{x} -> {y}' for x, y in zip(item_fastner, item_fastner_sort)]

        # Initialize a list to store total box weights
        total_box_weights = []

        # Index to keep track of the items
        item_index0 = 0

        # Loop to process each box
        for i in range(box_inc):
            # Calculate total weight of items for the current box
            total_item_weight = 0
            items_in_box = []

            num_items_in_box = box_item_counts[i] if i < len(box_item_counts) else 0

            for j in range(num_items_in_box):
                if item_index0 < len(item_weight):
                    total_item_weight += item_weight[item_index0]
                    items_in_box.append(item_weight[item_index0])
                    item_index0 += 1

            total_box_weight = round((box_weight[i] or 0) + total_item_weight, 2)  # Calculate total box weight
            total_box_weights.append(total_box_weight)

        # Save box details and track boxes
        boxes = []
        for i in range(box_inc):
            box = Box.objects.create(
                box_type=box_type[i],
                box_no=box_no[i],
                radio_type=box_radio_type[i],
                ma_code=box_ma_code[i],
                fastner=box_fastner_new[i],
                length=box_length[i],
                qty=box_qty[i],
                weight=box_weight[i],
                tot_box_weight=total_box_weights[i]
            )
            entry.boxes.add(box)
            boxes.append(box)

        # Index to keep track of the items
        item_index = 0

        # Loop to process each box
        for i in range(box_inc):
            # Calculate total weight of items for the current box
            total_item_weight = 0
            items_in_box = []

            # Retrieve the number of items to be added to this box
            num_items_in_box = box_item_counts[i] if i < len(box_item_counts) else 0

            for j in range(num_items_in_box):
                if item_index < len(item_radio_type):
                    item = Item.objects.create(
                        radio_type=item_radio_type[item_index],
                        ma_code=item_ma_code[item_index],
                        fastner=item_fastner_new[item_index],
                        length=item_length[item_index],
                        qty=item_qty[item_index],
                        weight=item_weight[item_index],
                    )
                    # Add item directly to the current box using index
                    boxes[i].items.add(item)  # Use i instead of box_index to match current box

                    items_in_box.append(item)
                    total_item_weight += item_weight[item_index]  # Accumulate item weight
                    item_index += 1

        return redirect('stockins')  # Redirect to the appropriate URL after successful submission
    else:
        return render(request, 'StockInsert.html')


@login_required
def stock_entryshow(request):
    all_data = Entry.objects.all()

    unique_boxes_count_list = []
    for entry in all_data:
        unique_box_numbers = set(box.box_no for box in entry.boxes.all())
        unique_boxes_count_list.append(len(unique_box_numbers))

    context = {
        'all_data': all_data,
        'unique_boxes_count_list': unique_boxes_count_list,
    }
    return render(request, 'stock_entryshow.html', context)


def view_boxes(request, pk):
    entry = Entry.objects.get(sr=pk)  # Fetch the Entry object
    boxes_data = entry.boxes.all()

    qr_codes = []
    for box in boxes_data:
        # Generate QR code for each box
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        details = [
            f'Client Name: {entry.client_name}',
            f'DC No: {entry.dc_no}',
            f'Delivery Address: {entry.delivery_add}',
            f'Date: {entry.date}',
            f'Box No: {box.box_no}',
            f'MA-Code: {box.ma_code}' if box.radio_type == "ma-code" else '',
            f'Fastner: {box.fastner}' if box.radio_type != "ma-code" else '',
            f'Length: {box.length}',
            f'Qty: {box.qty}',
        ]

        # Prepare item details for QR code
        items_data = box.items.all()
        items_details = []

        for idx, item in enumerate(items_data):
            if box.radio_type == "ma-code":
                item_description = item.ma_code
            else:
                item_description = item.fastner

            item_details = [
                f'Item {idx}:',
                f'  Fastner: {item_description}',
                f'  Length: {item.length}',
                f'  Qty: {item.qty}',
            ]
            items_details.append('\n'.join(item_details))

        # Join the details with line breaks and add to the QR code
        qr_data = '\n'.join(detail for detail in details if detail)
        if items_details:
            qr_data += '\n\n' + '\n\n'.join(items_details)

        qr.add_data(qr_data)

        qr.make(fit=True)
        img = qr.make_image(fill='black', back_color='white')

        buffered = BytesIO()
        img.save(buffered, format("PNG"))
        qr_code = base64.b64encode(buffered.getvalue()).decode('utf-8')
        qr_codes.append(qr_code)

    combined_data = list(zip(boxes_data, qr_codes))

    context = {
        'entry': entry,
        'boxes_data': boxes_data,
        'combined_data': combined_data,
    }
    return render(request, 'view_boxes.html', context)


def deleteEntry(request, pk):
    ddata = Entry.objects.get(sr=pk)
    # deleting the column
    ddata.delete()
    return redirect('stock_entryshow')
