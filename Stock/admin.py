from django.contrib import admin
from .models import Data, Entry, Box, Fastners, Item

# Register your models here.
admin.site.register(Data)
admin.site.register(Entry)
admin.site.register(Box)
admin.site.register(Item)
admin.site.register(Fastners)
