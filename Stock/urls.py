from django.urls import path
from . import views

urlpatterns = [
    path('stock', views.stockLogin, name='stock'),
    path('stockins', views.stockins, name='stockins'),
    path('stockentry', views.stock_entry, name='stockentry'),
    path('stock_entryshow', views.stock_entryshow, name='stock_entryshow'),
    path('view_boxes/<int:pk>', views.view_boxes, name='view_boxes'),
    path('deleteEntry/<int:pk>', views.deleteEntry, name='deleteEntry'),
]
