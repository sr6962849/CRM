"""
URL configuration for DesignStatus project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
# line 19 and 20 are imported for urlpatterns one line 27

urlpatterns = [
    path('', include('Status.urls')),
    path('', include('PostSales.urls')),
    path('', include('Stock.urls')),
    path('', include('NewCrm.urls')),
    path('', include('NewPs.urls')),
    path('', include('International.urls')),
    path('admin/', admin.site.urls),
]
urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# these things are necessary to for file handing in database means if we are
# adding and file in database
